# <link rel = "stylesheet" type = "text/css" href = "retro.css" /> ImGuiGML COOK BOOK 

The [ImGuiGML] Cook Book aims to be a collection of easy to search "recipes" of common UI patterns and techniques that a programmer may come across while developing Debug UIs or tools with [ImGuiGML].

## Contents [#Top]

* [**Getting Started**](#GettingStarted)
  *  [Use the experimental mem functions](#Mem)
* [**Techniques**](#Techniques)
  *  [How to make an element stretch-to-fit the window](#STFWin)
  *  [How to Place Multiple Panes in one Window](#WPanes)
  *  [How to Make a Full App Menu Bar](#WFMMenuBar)
  *  [How to Make a Selectable List](#T_SLIST)
* [**Trouble-Shooting and Problem Solving**](#TSandPS)
  *  [ImGuiGML silently crashes?](#TSSCrashes)
  *  [`imguigml_image` appears to draw garbage?](#TSImgSurf)
  
----

### Getting Started [#GettingStarted]

The most basic "block" of ImGui is the window system, created using a pair of calls to `imguigml_begin()` and `imguigml_end()`. `imguigml_begin()` is actually a bit of a powerful function that allows you a bunch of customization on your window through using the `EImGui_WindowFlag` flags.

Depending on how you're developing your ImGui scripts, there's two patterns you could follow with your begin/end pairs.

```
var _ret = imguigml_begin("My Window");

// The window is collapsed
if (!_ret[0]) {
  imguigml_end();
  return;
}

// Draw some window

imguigml_end();
```

or 

```
var _ret = imguigml_begin("My Window");
if (_ret[0]) {
  // The window isn't collapsed
  // Draw some window
}
imguigml_end();
```

The important note is that `imguigml_end` needs to be called if you've called `imguigml_begin` otherwise you'll have errors in your windows, or a default window will draw.

##### ImGuiGML Ready

One special case you might need to consider is if your going to start an ImGui window right when your game starts, and it may conflict with the creation order of ImGui. If that's the case, you'll want to guard your code with `imguigml_ready()`:

```
if (!imguigml_ready())
  return;
```

Early out in the event your script fired before ImGui has initialized.

[Top]

---

#### Use the experimental mem functions [#Mem]

imguigml currently strays a way from the native imgui storage
in favor of a `ds_map`. Occasionally you'll want to store the state of variables
in your imguigml scripts. Instead of creating instance variables 
you may use the experimental mem functions.

You may get a memory variable without ever having set it before - and supply it's default value.

```
var _label = "Text Input",
    _value = imguigml_mem(_label+"##value", "This is a default value");
    _input = imguigml_text_input(_label, _value, 128);
  
if(_input[0])){
  imguigml_memset(_label+"##value", _input[1]);
}
```

These functions are not yet context aware, and may be changed in the future.

[Top]

---

## Techniques [#Techniques]

Now that you've got your feet wet with ImGui, there may be some specific menu / UI styles you're trying to achieve - generally things that will have a more advanced setup.

[Top]

---

#### How to make an element stretch-to-fit the window [#STFWin]

To retrieve the maximum space you can draw to use: 

*  `{Real}  = imguigml_get_content_region_avail_width()` 
*  `{Array} = imguigml_get_content_region_avail()`

You can then use these sizes to push the item width:

```
imguigml_push_item_width(imguigml_get_content_region_avail_width());
// your control
imguigml_pop_item_width();
```

Or fill in size parameters in other [ImGuiGML] functions:

```
var avail = imguigml_get_content_region_avail();
imguigml_begin_child("Top Half", 0, avail[1] * 0.5)
```

_**Note:** `imguigml_begin_child`'s size parameter of 0 means "fit to max available space"_

[Techniques] - [Top]

---

#### How to Place Multiple Panes in One Window [#WPanes]

The following makes 4 panes adjacent to each other

```
imguigml_begin();

var avail = imguigml_get_content_region_avail();
imguigml_begin_child("Top Left", avail[0] * 0.5, avail[1] * 0.5)
imguigml_end_child();
imguigml_sameline();
imguigml_begin_child("Top Right", 0, avail[1] * 0.5);
imguigml_end_child();

avail = imguigml_get_content_region_avail();
imguigml_begin_child("Bottom Left", avail[0] * 0.5);
imguigml_end_child();
imguigml_sameline();
imguigml_begin_child("Bottom Right")
imguigml_end_child();

imguigml_end();
```

[Techniques] - [Top]

---

#### Make a "Windows File Menu" Style Menu Bar [#WFMMenuBar]

```
imguigml_set_next_window_pos(0, 0);
imguigml_set_next_window_size(display_get_gui_width(), imguigml_get_font_size());
imguigml_push_style_var(EImGui_StyleVar.WindowRounding, 0);
imguigml_push_style_color(EImGui_Col.WindowBg, 0, 0, 0, 0);
imguigml_begin("Unique Window Title", undefined, EImGui_WindowFlags.MenuBar|EImGui_WindowFlags.NoCollapse|EImGui_WindowFlags.NoMove|EImGui_WindowFlags.NoResize|EImGui_WindowFlags.NoTitleBar);
if (imguigml_begin_menu_bar()) {
  // menu / menu bar
}
imguigml_end_menu_bar();
imguigml_end();

imguigml_pop_style_var();
imguigml_pop_style_color();
```

[Techniques] - [Top]

---

#### How to Make a Selectable List [#T_SLIST]

```
	imguigml_list_box_header("##unique_tag", _width, _height);
	for (var _i = 0; _i < _num_items; ++_i) {
		var _item = _list_of_items[_i];
    var _item_name = item[EItem.Name]; // get a string
		var _is_selected = _i == selected_index;
		_ret = imguigml_selectable(_item_name, _is_selected);
		if (_ret[0] && _ret[1]) {
			selected_index = _i;
		}
	}
	imguigml_list_box_footer();
```

[Techniques] - [Top]

---

## Trouble-Shooting and Problem Solving [#TSandPS]

Fix it please.


#### ImGuiGML silently crashes? [#TSSCrashes]

Unfortunately, we're still trying to catch all the error cases that can cause crashes and figure out the best way to prevent AND report the errors. 
In the mean time, make sure:

*  All Begins must have Ends.
*  With `imguigml_begin_menu_bar` ... (I think this is the one that crashes if you end it after it returns false?)

---

#### `imguigml_image` appears to draw garbage when drawing surfaces? [#TSImgSurf]

You're gonna kick yourself: draw TO your surface in your draw event.

[Top]

-----

[Top]:      #Top
[Techniques]: #Techniques
[ImGuiGML]: http://imguigml.rou.sr/


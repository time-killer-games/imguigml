## Developing ImGui

### Generating the Solutions

First download `cmake` on your preferred platform - I use the gui version of `cmake` regardless of platform, I find it easiest to use. 

Point the "Source Directory" to `<path-to-imguigml>/dll` and the "Build Directory" to `<path-to-imguigml>/dll/solutions`. The `solutions` folder is already included in the `.gitignore` file, which keeps things clean and tidy if you're pushing your own commits to a fork. `Configure` the project next:

*   For Windows, you'll want to create either `Win32` or `x64` depending on what you're building. I haven't solved the issue of "I can't generate a solution with support for BOTH?!" - Build in "Release" primarily, but it's fairly easy to attach to the runner with the debugger.
*   For macOS, you'll just want to make sure the `CMAKE_OSX_ARCHITECTURES` is set to `(ARCHS_STANDARD)` - "Archive" build builds in Release automatically.
*   For Ubuntu, you'll primarily want to set the configuration field to `RELEASE` when building, but `DEBUG` also works if you fancy yourself some IM_ASSERTS (or can figure out how to attach GBD to it).

### Some Tips

I'll provide more tips as I think of 'em, but the primary one is to set your GMS 2 Text Editor to 2 space tabs. I don't know what happened, but the project has a mix of tabs and spaces used throughout and looks simply awful with 4 space tabs. I'm sorry.


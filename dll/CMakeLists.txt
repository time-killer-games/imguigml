cmake_minimum_required(VERSION 3.7.2)
project(Imguigml)

#set (CMAKE_CXX_STANDARD 11)
#set (CMAKE_CXX_STANDARD_REQUIRED ON)
#set (CMAKE_CXX_EXTENSIONS OFF)

include_directories(. vendor/ src/)

#grab the sources via wildcard
file(GLOB files "src/*.cpp")
file(GLOB files_headers "src/*.h")

file(GLOB rousrgml "src/rousrGML/*.cpp")
file(GLOB rousrgml_headers "src/rousrGML/*.h")

file(GLOB wrappers "src/imgui wrappers/*.cpp")
file(GLOB wrappers_headers "src/imgui wrappers/*.h")

file(GLOB imgui "vendor/imgui/*.cpp")
file(GLOB imgui_headers "vendor/imgui/*.h")

file(GLOB textedit "vendor/imguicolortextedit/*.cpp")
file(GLOB textedit_headers "vendor/imguicolortextedit/*.h")

set(SOURCES ${files} ${files_headers} ${rousrgml} ${rousrgml_headers} ${wrappers} ${wrappers_headers} ${imgui} ${imgui_headers} ${textedit} ${textedit_headers})

if (UNIX AND NOT APPLE)
    add_library(Imguigml MODULE ${SOURCES})
else()
    add_library(Imguigml SHARED ${SOURCES})
endif(UNIX AND NOT APPLE)

source_group("rousrgml" FILES ${rousrgml})
source_group("imguigml" FILES ${files})
source_group("imguigml/wrappers" FILES ${wrappers})
source_group("vendor/imgui" FILES ${imgui})
source_group("vendor/textedit" FILES ${textedit})

source_group("rousrgml" FILES ${rousrgml_headers})
source_group("imguigml" FILES ${files_headers})
source_group("imguigml/wrappers" FILES ${wrappers_headers})
source_group("vendor/imgui" FILES ${imgui_headers})
source_group("vendor/textedit" FILES ${textedit_headers})

set_target_properties(Imguigml PROPERTIES PREFIX "")

if (APPLE)
    set( CMAKE_XCODE_ATTRIBUTE_ARCHS            "$(ARCHS_STANDARD)" ) # http://www.cocoanetics.com/2014/10/xcode-6-drops-armv7s
    set( CMAKE_XCODE_ATTRIBUTE_VALID_ARCHS      "$(ARCHS_STANDARD)" )
    set( CMAKE_XCODE_ATTRIBUTE_ONLY_ACTIVE_ARCH NO                  )
    target_link_libraries(Imguigml "-framework CoreFoundation")
    target_link_libraries(Imguigml "-framework ApplicationServices")
    target_link_libraries(Imguigml "-arch x86_64")
    target_link_libraries(Imguigml "-arch arm64")
    set_target_properties(Imguigml PROPERTIES COMPILE_FLAGS "-std=c++14")
endif(APPLE)

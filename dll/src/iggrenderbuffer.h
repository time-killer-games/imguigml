#pragma once

#include "types.h"

#include "rousrGML/GMLBuffer.h"

struct ImDrawData;

class IGGRenderBuffer {
public:
    IGGRenderBuffer(char *_cmdBuffer, uint32_t _cmdBufferSize, char *_vertexBuffer, uint32_t _vertexBufferSize)
        : mCmdBuffer(_cmdBufferSize, _cmdBuffer)
        , mVertexBuffer(_vertexBufferSize, _vertexBuffer)
    { ; }

    uint32_t WriteBuffer(ImDrawData* _drawData);

    uint32_t GetLastVertBuffSize() const { return mLastVertBuffSize; }
    uint32_t GetLastCommandCount() const { return mLastCommandCount; }

private:
    CGMLBuffer mCmdBuffer;
    CGMLBuffer mVertexBuffer;

    uint32_t mLastVertBuffSize = 0;
    uint32_t mLastCommandCount = 0;
};
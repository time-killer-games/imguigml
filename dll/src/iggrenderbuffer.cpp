#include "imguigml.h"

#include "iggrenderbuffer.h"

#include "imgui/imgui.h"

uint32_t IGGRenderBuffer::WriteBuffer(ImDrawData* _drawData) {
    if (_drawData == nullptr)
        return 0;

    _drawData->DeIndexAllBuffers();
    mVertexBuffer.Seek(0);
    mCmdBuffer.Seek(0);

    uint32_t vertBuffSize(_drawData->TotalVtxCount * (sizeof(ImDrawVert) + 4 + 4 + 4));
    for (int32_t cmdListIndex(0); cmdListIndex < _drawData->CmdListsCount; ++cmdListIndex) {
        auto cmdList(_drawData->CmdLists[cmdListIndex]);
        
  //      mVertexBuffer.WriteData(reinterpret_cast<char*>(cmdList->VtxBuffer.Data), cmdList->VtxBuffer.Size * sizeof(ImDrawVert));
    
        for (int32_t vertIndex(0), vertEnd(cmdList->VtxBuffer.Size); vertIndex < vertEnd; ++vertIndex) {
            const auto& vert(cmdList->VtxBuffer[vertIndex]);
            mVertexBuffer.Write<float>(vert.pos.x);
            mVertexBuffer.Write<float>(vert.pos.y);
            mVertexBuffer.Write<float>(vert.uv.x);
            mVertexBuffer.Write<float>(vert.uv.y);
            //mVertexBuffer.Write<uint32_t>(vert.col);
            mVertexBuffer.Write<float>((vert.col >> IM_COL32_R_SHIFT & 0xFF) / 255.0f);
            mVertexBuffer.Write<float>((vert.col >> IM_COL32_G_SHIFT & 0xFF) / 255.0f);
            mVertexBuffer.Write<float>((vert.col >> IM_COL32_B_SHIFT & 0xFF) / 255.0f);
            mVertexBuffer.Write<float>((vert.col >> IM_COL32_A_SHIFT & 0xFF) / 255.0f);
        }

        for (int32_t cmdIndex(0); cmdIndex < cmdList->CmdBuffer.Size; ++cmdIndex) {
            const auto cmd(&cmdList->CmdBuffer[cmdIndex]);
            
            // UserCallbacks are unlikely ever to be used with ImGuiGML, nut just in case.
            if (cmd->UserCallback != nullptr) 
                cmd->UserCallback(cmdList, cmd);
            else {
                mCmdBuffer.Write<uint8_t>(1);
                mCmdBuffer.Write<uint32_t>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(cmd->TextureId)));
                mCmdBuffer.Write<uint32_t>(cmd->ElemCount);

                mCmdBuffer.Write(cmd->ClipRect.x);
                mCmdBuffer.Write(cmd->ClipRect.y);
                mCmdBuffer.Write(cmd->ClipRect.z);
                mCmdBuffer.Write(cmd->ClipRect.w);
            }
        }
    }

    mCmdBuffer.Write<uint8_t>(0);

    return vertBuffSize;
}
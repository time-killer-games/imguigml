#pragma once

#if defined(_MSC_VER)
    #define DLL_API __declspec(dllexport)
#elif defined(__GNUC__)
    #define DLL_API __attribute__((visibility("default")))
#else
    #define DLL_API
#endif

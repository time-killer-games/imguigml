#include "imguigml_wrappers.h"

extern "C" {
    ///////////
    // Widgets: Sliders (tip: ctrl+click on a slider to input with keyboard. manually input values aren't clamped, can go off-bounds)
    ///////////

    ////
    // IMGUI_API bool          SliderFloat(const char* label, float* v, float v_min, float v_max, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    //  adjust display_format to decorate the value with a prefix or a suffix for in-slider labels or unit display.
    IMGML SliderFloat() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        float v(In_ImGML_Float());
        float vMin(In_ImGML_Float());
        float vMax(In_ImGML_Float());
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::SliderFloat(label.c_str(), &v, vMin, vMax, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v);
        return 1.0;
    }

    ////
    //IMGUI_API bool          SliderFloat2(const char* label, float v[2], float v_min, float v_max, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    IMGML SliderFloat2() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        float v[2] = { 0.0f, 0.0f };
        v[0] = In_ImGML_Float();
        v[1] = In_ImGML_Float();
        float vMin(In_ImGML_Float());
        float vMax(In_ImGML_Float());
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::SliderFloat2(label.c_str(), v, vMin, vMax, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v[0]);
        Out_ImGML_Float(v[1]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          SliderFloat3(const char* label, float v[3], float v_min, float v_max, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    IMGML SliderFloat3() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        float v[3] = { 0.0f, 0.0f, 0.0f };
        v[0] = In_ImGML_Float();
        v[1] = In_ImGML_Float();
        v[2] = In_ImGML_Float();
        float vMin(In_ImGML_Float());
        float vMax(In_ImGML_Float());
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::SliderFloat3(label.c_str(), v, vMin, vMax, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v[0]);
        Out_ImGML_Float(v[1]);
        Out_ImGML_Float(v[2]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          SliderFloat4(const char* label, float v[4], float v_min, float v_max, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    IMGML SliderFloat4() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        float v[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
        v[0] = In_ImGML_Float();
        v[1] = In_ImGML_Float();
        v[2] = In_ImGML_Float();
        v[3] = In_ImGML_Float();
        float vMin(In_ImGML_Float());
        float vMax(In_ImGML_Float());
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::SliderFloat4(label.c_str(), v, vMin, vMax, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v[0]);
        Out_ImGML_Float(v[1]);
        Out_ImGML_Float(v[2]);
        Out_ImGML_Float(v[3]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          SliderAngle(const char* label, float* v_rad, float v_degrees_min = -360.0f, float v_degrees_max = +360.0f);
    IMGML SliderAngle() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        float v_rad(In_ImGML_Float());
        float vMin(In_ImGML_Float());
        float vMax(In_ImGML_Float());
        bool ret(ImGui::SliderAngle(label.c_str(), &v_rad, vMin, vMax));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v_rad);
        return 1.0;
    }

    ////
    //IMGUI_API bool          SliderInt(const char* label, int* v, int v_min, int v_max, const char* display_format = "%.0f");
    IMGML SliderInt() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        int v(In_ImGML_Int());
        int vMin(In_ImGML_Int());
        if (vMin == 2000) {
            int x = 0;
            x++;
        }
        int vMax(In_ImGML_Int());
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        bool ret(ImGui::SliderInt(label.c_str(), &v, vMin, vMax, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v);
        
        return 1.0;
    }

    ////
    //IMGUI_API bool          SliderInt2(const char* label, int v[2], int v_min, int v_max, const char* display_format = "%.0f");
    IMGML SliderInt2() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        int v[2] = { 0, 0 };
        v[0] = In_ImGML_Int();
        v[1] = In_ImGML_Int();
        int vMin(In_ImGML_Int());
        int vMax(In_ImGML_Int());
        std::string displayFmt(In_ImGML_StringOpt("%.0f"));
        bool ret(ImGui::SliderInt2(label.c_str(), v, vMin, vMax, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v[0]);
        Out_ImGML_Int(v[1]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          SliderInt3(const char* label, int v[3], int v_min, int v_max, const char* display_format = "%.0f");
    IMGML SliderInt3() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        int v[3] = { 0, 0, 0 };
        v[0] = In_ImGML_Int();
        v[1] = In_ImGML_Int();
        v[2] = In_ImGML_Int();
        int vMin(In_ImGML_Int());
        int vMax(In_ImGML_Int());
        std::string displayFmt(In_ImGML_StringOpt("%.0f"));
        bool ret(ImGui::SliderInt3(label.c_str(), v, vMin, vMax, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v[0]);
        Out_ImGML_Int(v[1]);
        Out_ImGML_Int(v[2]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          SliderInt4(const char* label, int v[4], int v_min, int v_max, const char* display_format = "%.0f");
    IMGML SliderInt4() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        int v[4] = { 0, 0, 0, 0 };
        v[0] = In_ImGML_Int();
        v[1] = In_ImGML_Int();
        v[2] = In_ImGML_Int();
        v[3] = In_ImGML_Int();
        int vMin(In_ImGML_Int());
        int vMax(In_ImGML_Int());
        std::string displayFmt(In_ImGML_StringOpt("%.0f"));
        bool ret(ImGui::SliderInt4(label.c_str(), v, vMin, vMax, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v[0]);
        Out_ImGML_Int(v[1]);
        Out_ImGML_Int(v[2]);
        Out_ImGML_Int(v[3]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          VSliderFloat(const char* label, const ImVec2& size, float* v, float v_min, float v_max, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    IMGML VSliderFloat() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());
        float v(In_ImGML_Float());
        float vMin(In_ImGML_Float());
        float vMax(In_ImGML_Float());
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::VSliderFloat(label.c_str(), ImVec2(x, y), &v, vMin, vMax, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v);
        return 1.0;
    }

    ////
    //IMGUI_API bool          VSliderInt(const char* label, const ImVec2& size, int* v, int v_min, int v_max, const char* display_format = "%.0f");
    IMGML VSliderInt() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());
        int v(In_ImGML_Int());
        int vMin(In_ImGML_Int());
        int vMax(In_ImGML_Int());
        std::string displayFmt(In_ImGML_StringOpt("%.0f"));
        bool ret(ImGui::VSliderInt(label.c_str(), ImVec2(x, y), &v, vMin, vMax, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v);
        return 1.0;
    }
}
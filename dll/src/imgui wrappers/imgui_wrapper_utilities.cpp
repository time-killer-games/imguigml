#include "imguigml_wrappers.h"

extern "C" {
    //////////////
    // Utilities
    //////////////
    
    ////
    // IMGUI_API bool          IsItemHovered(ImGuiHoveredFlags flags = 0);
    //  is the last item hovered by mouse (and usable)?
    IMGML IsItemHovered() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsItemHovered(In_ImGML_IntOpt(0)));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsItemActive();   
    //  is the last item active? (e.g. button being held, text field being edited- items that don't interact will always return false)
    IMGML IsItemActive() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::IsItemActive());
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsItemClicked(int mouse_button = 0);    
    //  is the last item clicked? (e.g. button/node just clicked on)
    IMGML IsItemClicked() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsItemClicked(In_ImGML_IntOpt(0)));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsItemVisible();                      
    //  is the last item visible? (aka not out of sight due to clipping/scrolling.)
    IMGML IsItemVisible() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::IsItemVisible());
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsAnyItemHovered();
    IMGML IsAnyItemHovered() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::IsAnyItemHovered());
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsAnyItemActive();
    IMGML IsAnyItemActive() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::IsAnyItemActive());
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetItemRectMin();                 
    //  get bounding rect of last item in screen space
    IMGML GetItemRectMin() {
        CheckImGML_Out();
        ImVec2 rect(ImGui::GetItemRectMin());
        Out_ImGML_Float(rect.x);
        Out_ImGML_Float(rect.y);
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetItemRectMax();             
    IMGML GetItemRectMax() {
        CheckImGML_Out();
        ImVec2 rect(ImGui::GetItemRectMax());
        Out_ImGML_Float(rect.x);
        Out_ImGML_Float(rect.y);
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetItemRectSize();             
    IMGML GetItemRectSize() {
        CheckImGML_Out();
        ImVec2 rect(ImGui::GetItemRectSize());
        Out_ImGML_Float(rect.x);
        Out_ImGML_Float(rect.y);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetItemAllowOverlap();          
    //  allow last item to be overlapped by a subsequent item. sometimes useful with invisible buttons, selectables, etc. to catch unused area.
    IMGML SetItemAllowOverlap() {
        CheckImGML();
        ImGui::SetItemAllowOverlap();
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsWindowFocused(ImGuiFocusedFlags _flags);                 
    //  is current Begin()-ed window focused?
    IMGML IsWindowFocused() {
        CheckImGML_InOut();
        auto flags(In_ImGML_Uint());
        Out_ImGML_Bool(ImGui::IsWindowFocused(flags));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsWindowHovered(ImGuiHoveredFlags flags = 0);          
    //  is current Begin()-ed window hovered (and typically: not blocked by a popup/modal)?
    IMGML IsWindowHovered() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsWindowHovered(In_ImGML_IntOpt(0)));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsRectVisible(const ImVec2& size);        
    //  test if rectangle (of given size, starting from cursor position) is visible / not clipped.
    // IMGUI_API bool          IsRectVisible(const ImVec2& rect_min, const ImVec2& rect_max); 
    //  test if rectangle (in screen space) is visible / not clipped. to perform coarse clipping on user's side.
    IMGML IsRectVisible() {
        CheckImGML_InOut();
        float minX(In_ImGML_Float());
        float minY(In_ImGML_Float());
        bool ret(false);
        if (in.Count() > 2) {
            float maxX(In_ImGML_Float());
            float maxY(In_ImGML_Float());

            ret = ImGui::IsRectVisible(ImVec2(minX, minY), ImVec2(maxX, maxY));
        } else {
            ret = ImGui::IsRectVisible(ImVec2(minX, minY));
        }

        Out_ImGML_Bool(ret);
        return 1.0;
    }

    ////
    // IMGUI_API double        GetTime();
    IMGML GetTime() {
        CheckImGML_Out();
        Out_ImGML_Double(ImGui::GetTime());
        return 1.0;
    }

    ////
    // IMGUI_API int           GetFrameCount();
    IMGML GetFrameCount() {
        CheckImGML_Out();
        Out_ImGML_Int(ImGui::GetFrameCount());
        return 1.0;
    }

    ////
    // IMGUI_API const char*   GetStyleColorName(ImGuiCol idx);
    IMGML GetStyleColorName() {
        CheckImGML_InOut();
        Out_ImGML_String(ImGui::GetStyleColorName(In_ImGML_Int()));
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        CalcTextSize(const char* text, const char* text_end = NULL, bool hide_text_after_double_hash = false, float wrap_width = -1.0f);
    IMGML CalcTextSize() {
        CheckImGML_InOut();
        std::string strTextEnd("");
        const char* textEnd(nullptr);

        auto text(In_ImGML_String());
        if (in.Count() > 1 && in.IsType<std::string>(1)) {
            strTextEnd = In_ImGML_String();
            textEnd = strTextEnd.c_str();
        } else
            In_ImGML_BoolOpt(false);
        
        bool hideTextAfterDoubleDash(In_ImGML_BoolOpt(false));
        float wrapWidth(In_ImGML_FloatOpt(-1.0f));
        ImVec2 size(ImGui::CalcTextSize(text.c_str(), textEnd, hideTextAfterDoubleDash, wrapWidth));

        return 1.0;
    }

    ////
    // IMGUI_API void          CalcListClipping(int items_count, float items_height, int* out_items_display_start, int* out_items_display_end);
    //  calculate coarse clipping for large list of evenly sized items. Prefer using the ImGuiListClipper higher-level helper if you can.
    IMGML CalcListClipping() {
        CheckImGML_InOut();
        int itemsCount(In_ImGML_Int());
        float itemsHeight(In_ImGML_Float());
        int itemsStart(0), itemsEnd(0);
        ImGui::CalcListClipping(itemsCount, itemsHeight, &itemsStart, &itemsEnd);
        Out_ImGML_Int(itemsStart);
        Out_ImGML_Int(itemsEnd);
        return 1.0;
    }

    ////
    // IMGUI_API ImVec4        ColorConvertU32ToFloat4(ImU32 in);
    IMGML ColorConvertU32ToFloat4() {
        CheckImGML_InOut();
        auto color(In_ImGML_Uint());
        ImVec4 fcol(ImGui::ColorConvertU32ToFloat4(color));
        Out_ImGML_Float(fcol.x);
        Out_ImGML_Float(fcol.y);
        Out_ImGML_Float(fcol.z);
        Out_ImGML_Float(fcol.w);
        return 1.0;
    }

    ////
    // IMGUI_API ImU32         ColorConvertFloat4ToU32(const ImVec4& in);
    IMGML ColorConvertFloat4ToU32() {
        CheckImGML_InOut();
        auto r(In_ImGML_Float());
        auto g(In_ImGML_Float());
        auto b(In_ImGML_Float());
        auto a(In_ImGML_Float());
        Out_ImGML_Uint(ImGui::ColorConvertFloat4ToU32(ImVec4(r, g, b, a)));
        return 1.0;
    }

    ////
    // IMGUI_API void          ColorConvertRGBtoHSV(float r, float g, float b, float& out_h, float& out_s, float& out_v);
    IMGML ColorConvertRGBtoHSV() {
        CheckImGML_InOut();
        float r(In_ImGML_Float());
        float g(In_ImGML_Float());
        float b(In_ImGML_Float());
        float h(0.0f), s(0.0f), v(0.0f);
        ImGui::ColorConvertRGBtoHSV(r,g,b,h,s,v); 
        Out_ImGML_Float(h);
        Out_ImGML_Float(s);
        Out_ImGML_Float(v);
        return 1.0;
    }

    ////
    // IMGUI_API void          ColorConvertHSVtoRGB(float h, float s, float v, float& out_r, float& out_g, float& out_b);
    IMGML ColorConvertHSVtoRGB() {
        CheckImGML_InOut();
        float h(In_ImGML_Float());
        float s(In_ImGML_Float());
        float v(In_ImGML_Float());
        float r(0.0f), g(0.0f), b(0.0f);
        ImGui::ColorConvertHSVtoRGB(h,s,v,r,g,b);
        Out_ImGML_Float(r);
        Out_ImGML_Float(g);
        Out_ImGML_Float(b);
        return 1.0;
    }

    //////////////
    // Widgets: Value() Helpers. Output single value in "name: value" format (tip: freely declare more in your code to handle your types. you can add functions to the ImGui namespace)
    //////////////

    ////
    // IMGUI_API void          Value(const char* prefix, bool b);
    // IMGUI_API void          Value(const char* prefix, int v);
    // IMGUI_API void          Value(const char* prefix, unsigned int v);
    // IMGUI_API void          Value(const char* prefix, float v, const char* float_format = NULL);
    IMGML Value() {
        CheckImGML_In();
        auto prefix(In_ImGML_String());
        if (in.NextType<int8_t>())
            ImGui::Value(prefix.c_str(), In_ImGML_Bool());
        else if (in.NextType<int32_t>())
            ImGui::Value(prefix.c_str(), In_ImGML_Int());
        else if (in.NextType<uint32_t>())
            ImGui::Value(prefix.c_str(), In_ImGML_Uint());
        else if (in.NextType<float>()) {
            float val(In_ImGML_Float());
            std::string strFloatFmt("");
            const char* floatFmt(nullptr);
            if (in.NextType<std::string>()) {
                strFloatFmt = In_ImGML_String();
                floatFmt = strFloatFmt.c_str();
            }
           
            ImGui::Value(prefix.c_str(), val, floatFmt);
        }
        
        return 1.0;
    }

    ////////////
    // Clipping
    ////////////

    ////
    // IMGUI_API void          PushClipRect(const ImVec2& clip_rect_min, const ImVec2& clip_rect_max, bool intersect_with_current_clip_rect);
    IMGML PushClipRect() {
        CheckImGML_In();
        float minX(In_ImGML_Float());
        float minY(In_ImGML_Float());
        float maxX(In_ImGML_Float());
        float maxY(In_ImGML_Float());
        bool intersectWithCurrentClipRect(In_ImGML_Bool());
        ImGui::PushClipRect(ImVec2(minX, maxX), ImVec2(maxX, maxY), intersectWithCurrentClipRect);
        return 1.0;
    }

    ////
    // IMGUI_API void          PopClipRect();
    IMGML PopClipRect() {
        CheckImGML();
        ImGui::PopClipRect();
        return 1.0;
    }

}

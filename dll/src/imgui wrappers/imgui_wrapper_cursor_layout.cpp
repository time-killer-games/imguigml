#include "imguigml_wrappers.h"

extern "C" {
    ///////////
    // Cursor / Layout
    ///////////

    ////
    // IMGUI_API void          Separator();
    //  separator, generally horizontal. inside a menu bar or in horizontal layout mode, this becomes a vertical separator.
    IMGML Separator() {
        CheckImGML();
        ImGui::Separator();
        return 1.0;
    }

    ////
    // IMGUI_API void          SameLine(float pos_x = 0.0f, float spacing_w = -1.0f);
    //  call between widgets or groups to layout them horizontally
    IMGML SameLine() {
        CheckImGML_In();
        float posX(In_ImGML_FloatOpt(0.0f));
        float spacingW(In_ImGML_FloatOpt(-1.0f));
        ImGui::SameLine(posX, spacingW);
        return 1.0;
    }

    ////
    // IMGUI_API void          NewLine();  
    //  undo a SameLine()
    IMGML NewLine() {
        CheckImGML();
        ImGui::NewLine();
        return 1.0;
    }
    
    ////
    // IMGUI_API void          Spacing();    
    //  add vertical spacing
    IMGML Spacing() {
        CheckImGML();
        ImGui::Spacing();
        return 1.0;
    }

    ////
    // IMGUI_API void          Dummy(const ImVec2& size);  
    //  add a dummy item of given size
    IMGML Dummy() {
        CheckImGML_In();
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());               
        ImGui::Dummy(ImVec2(x, y));
        return 1.0;
    }

    ////
    // IMGUI_API void          Indent(float indent_w = 0.0f);    
    //  move content position toward the right, by style.IndentSpacing or indent_w if >0
    IMGML Indent() {
        CheckImGML_In();
        ImGui::Indent(In_ImGML_FloatOpt(0.0f));
        return 1.0;
    }

    ////
    // IMGUI_API void          Unindent(float indent_w = 0.0f);      
    //  move content position back to the left, by style.IndentSpacing or indent_w if >0
    IMGML Unindent() {
        CheckImGML_In();
        ImGui::Unindent(In_ImGML_FloatOpt(0.0f));
        return 1.0;
    }

    ////
    // IMGUI_API void          BeginGroup();     
    //  lock horizontal starting position + capture group bounding box into one "item" (so you can use IsItemHovered() or layout primitives such as SameLine() on whole group, etc.)
    IMGML BeginGroup() {
        CheckImGML();
        ImGui::BeginGroup();
        return 1.0;
    }

    ////
    // IMGUI_API void          EndGroup();
    IMGML EndGroup() {
        CheckImGML();
        ImGui::EndGroup();
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetCursorPos();   
    //  cursor position is relative to window position
    IMGML ImGetCursorPos() {
        CheckImGML_Out();
        auto pos(ImGui::GetCursorPos());
        Out_ImGML_Float(pos.x);
        Out_ImGML_Float(pos.y);
        return 1.0;
    }

    ////
    // IMGUI_API float         GetCursorPosX();       
    IMGML GetCursorPosX() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetCursorPosX());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetCursorPosY(); 
    IMGML GetCursorPosY() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetCursorPosY());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetCursorPos(const ImVec2& local_pos);  
    IMGML ImSetCursorPos() {
        CheckImGML_In();
        auto x(In_ImGML_Float());
        auto y(In_ImGML_Float());
        ImGui::SetCursorPos(ImVec2(x, y));
        return 1.0;
    }

    ////
    // IMGUI_API void          SetCursorPosX(float x);                                             
    IMGML SetCursorPosX() {
        CheckImGML_In();
        ImGui::SetCursorPosX(In_ImGML_Float());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetCursorPosY(float y); 
    IMGML SetCursorPosY() {
        CheckImGML_In();
        ImGui::SetCursorPosY(In_ImGML_Float());
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetCursorStartPos();      
    //  initial cursor position
    IMGML GetCursorStartPos() {
        CheckImGML_Out();
        auto pos(ImGui::GetCursorStartPos());
        Out_ImGML_Float(pos.x);
        Out_ImGML_Float(pos.y);
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetCursorScreenPos();        
    //  cursor position in absolute screen coordinates [0..io.DisplaySize] (useful to work with ImDrawList API)
    IMGML GetCursorScreenPos() {
        CheckImGML_Out();
        auto pos(ImGui::GetCursorScreenPos());
        Out_ImGML_Float(pos.x);
        Out_ImGML_Float(pos.y);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetCursorScreenPos(const ImVec2& pos);   
    //  cursor position in absolute screen coordinates [0..io.DisplaySize]
    IMGML SetCursorScreenPos() {
        CheckImGML_In();
        auto x(In_ImGML_Float());
        auto y(In_ImGML_Float());
        ImGui::SetCursorScreenPos(ImVec2(x, y));
        return 1.0;
    }

    ////
    // IMGUI_API void          AlignTextToFramePadding();   
    //  vertically align/lower upcoming text to FramePadding.y so that it will aligns to upcoming widgets (call if you have text on a line before regular widgets)
    IMGML AlignTextToFramePadding() {
        CheckImGML();
        ImGui::AlignTextToFramePadding();
        return 1.0;
    }

    ////
    // IMGUI_API float         GetTextLineHeight();           
    //  height of font == GetWindowFontSize()
    IMGML GetTextLineHeight() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetTextLineHeight());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetTextLineHeightWithSpacing();        
    //  distance (in pixels) between 2 consecutive lines of text == GetWindowFontSize() + GetStyle().ItemSpacing.y
    IMGML GetTextLineHeightWithSpacing() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetTextLineHeightWithSpacing());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetFrameHeight();
    //   ~ FontSize + style.FramePadding.y * 2
    IMGML GetFrameHeight() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetFrameHeight());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetItemsLineHeightWithSpacing();           
    //  distance (in pixels) between 2 consecutive lines of standard height widgets == GetWindowFontSize() + GetStyle().FramePadding.y*2 + GetStyle().ItemSpacing.y
    IMGML GetFrameHeightWithSpacing() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetFrameHeightWithSpacing());
        return 1.0;
    }

    //////////////////////
    // Columns
    // You can also use SameLine(pos_x) for simplified columns. 
    // The columns API is still work-in-progress and rather lacking.
    ///////////

    ////
    // IMGUI_API void          Columns(int count = 1, const char* id = NULL, bool border = true);
    IMGML Columns() {
        CheckImGML_In();
        std::string idStr("");
        const char* id(nullptr);
        
        auto count(In_ImGML_IntOpt(1));
        if (in.Count() > 1 && in.IsType<std::string>(1)) {
            idStr = in.Val<std::string>(1);
            id = idStr.c_str();
        }
        bool border(in.Count() > 2 ? in.Val<int8_t>(2, 1) == 1 : true);

        ImGui::Columns(count, id, border);
        return 1.0;
    }

    ////
    // IMGUI_API void          NextColumn();                  
    //  next column, defaults to current row or next row if the current row is finished
    IMGML NextColumn() {
        CheckImGML();
        ImGui::NextColumn();
        return 1.0;
    }

    ////
    // IMGUI_API int           GetColumnIndex();           
    //  get current column index
    IMGML GetColumnIndex() {
        CheckImGML_Out();
        Out_ImGML_Int(ImGui::GetColumnIndex());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetColumnWidth(int column_index = -1);   
    //  get column width (in pixels). pass -1 to use current column
    IMGML GetColumnWidth() {
        CheckImGML_InOut();
        Out_ImGML_Float(ImGui::GetColumnWidth(In_ImGML_Int()));
        return 1.0;
    }

    ////
    // IMGUI_API void          SetColumnWidth(int column_index, float width);  
    //  set column width (in pixels). pass -1 to use current column
    IMGML SetColumnWidth() {
        CheckImGML_InOut();
        auto columnIndex(In_ImGML_Int());
        auto width(In_ImGML_Float());
        ImGui::SetColumnWidth(columnIndex, width);
        return 1.0;
    }

    ////
    // IMGUI_API float         GetColumnOffset(int column_index = -1);     
    //  get position of column line (in pixels, from the left side of the contents region). pass -1 to use current column, otherwise 0..GetColumnsCount() inclusive. column 0 is typically 0.0f
    IMGML GetColumnOffset() {
        CheckImGML_InOut();
        Out_ImGML_Float(ImGui::GetColumnOffset(In_ImGML_IntOpt(-1)));
        return 1.0;
    }

    ////
    // IMGUI_API void          SetColumnOffset(int column_index, float offset_x);    
    //  set position of column line (in pixels, from the left side of the contents region). pass -1 to use current column
    IMGML SetColumnOffset() {
        CheckImGML_In();
        auto columnIndex(In_ImGML_Int());
        auto offsetX(In_ImGML_Float());
        ImGui::SetColumnOffset(columnIndex, offsetX);
        return 1.0;
    }

    ////
    // IMGUI_API int           GetColumnsCount();
    IMGML GetColumnsCount() {
        CheckImGML_Out();
        Out_ImGML_Int(ImGui::GetColumnsCount());
        return 1.0;
    }
}
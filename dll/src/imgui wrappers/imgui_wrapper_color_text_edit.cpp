#include "imguigml_wrappers.h"
#include "imguicolortextedit/TextEditor.h"

#define CheckImGML_Text() \
    auto editorIndex(In_ImGML_Uint()); \
    std::shared_ptr<TextEditor> editorPtr(editorIndex < TextEditors.size() ? TextEditors[editorIndex] : nullptr); \
    if (editorPtr == nullptr) return -2.0; auto& editor(*editorPtr);

namespace {
    std::vector<std::shared_ptr<TextEditor>> TextEditors;
    std::stack<size_t> FreeIndices;

    std::map<std::string, TextEditor::LanguageDefinition> Langs;
    void GenerateLangMap() {
        if (!Langs.empty())
            return;

        { auto lang(TextEditor::LanguageDefinition::CPlusPlus()); Langs[lang.mName] = lang; }
        { auto lang(TextEditor::LanguageDefinition::HLSL()); Langs[lang.mName] = lang; }
        { auto lang(TextEditor::LanguageDefinition::GLSL()); Langs[lang.mName] = lang; }
        { auto lang(TextEditor::LanguageDefinition::C()); Langs[lang.mName] = lang; }
        { auto lang(TextEditor::LanguageDefinition::SQL()); Langs[lang.mName] = lang; }
        { auto lang(TextEditor::LanguageDefinition::Lua()); Langs[lang.mName] = lang; }
    }
}

extern "C" {
    IMGML TextEditorCreate() {
        CheckImGML_Out();
        GenerateLangMap();
        size_t index(static_cast<size_t>(~0));
        auto newEditor(std::make_shared<TextEditor>());
        if (!FreeIndices.empty()) {
            index = FreeIndices.top();
            FreeIndices.pop();
        } else {
            index = TextEditors.size();
            TextEditors.resize(index + 1);
        }
        TextEditors[index] = newEditor;
        Out_ImGML_Double(static_cast<double>(index));
        return 1.0;
    }

    IMGML TextEditorClose() {
        CheckImGML_In(); 
        auto editorIndex(static_cast<uint32_t>(In_ImGML_Double()));
        std::shared_ptr<TextEditor> editorPtr(editorIndex < TextEditors.size() ? TextEditors[editorIndex] : nullptr);
        if (editorPtr == nullptr)
            return -2.0;
        TextEditors[editorIndex].reset();
        FreeIndices.push(static_cast<size_t>(editorIndex));
        return 1.0;
    }

    IMGML TextEditorsCleanUp() {
        while (!FreeIndices.empty())
            FreeIndices.pop();
        TextEditors.clear();
        Langs.clear();
        return 1.0;
    }

    ////
    // void SetLanguageDefinition(const LanguageDefinition& aLanguageDef);
    IMGML TextEditorSetLanguageDefinition() {
        CheckImGML_In();
        CheckImGML_Text(); 
        auto lang(In_ImGML_String());
        auto itFindLang(Langs.find(lang));
        if (itFindLang == Langs.end())
            return -3.0;
        editor.SetLanguageDefinition(itFindLang->second);
        return 1.0;
    }
    
    ////
    // const LanguageDefinition& GetLanguageDefinition() const 
    IMGML TextEditorGetLanguageDefinition() {
        CheckImGML_InOut();
        CheckImGML_Text();
        Out_ImGML_String(editor.GetLanguageDefinition().mName);
        return 1.0;
    }

    ////
    // const Palette& GetPalette() const 
    IMGML TextEditorGetPalette() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto palette(editor.GetPalette());
        for (uint32_t paletteIndex(0); paletteIndex < static_cast<uint32_t>(TextEditor::PaletteIndex::Max); ++paletteIndex)
            Out_ImGML_Ubyte(palette[paletteIndex]);

        return 1.0;
    }

    ////
    // void SetPalette(const Palette& aValue);
    IMGML TextEditorSetPalette() {
        CheckImGML_InOut();
        CheckImGML_Text();
        TextEditor::Palette palette;
        for (uint32_t paletteIndex(0); paletteIndex < static_cast<uint32_t>(TextEditor::PaletteIndex::Max); ++paletteIndex)
            palette[paletteIndex] = In_ImGML_Ubyte();
        editor.SetPalette(palette);
        return 1.0;
    }

    ////
    //void SetErrorMarkers(const ErrorMarkers& aMarkers)
    ////
    //void SetBreakpoints(const Breakpoints& aMarkers)  

    ////
    // void Render(const char* aTitle, const ImVec2& aSize = ImVec2(), bool aBorder = false);
    IMGML TextEditorRender() {
        CheckImGML();
        CGMLTypedBuffer in(imguigml->WrapperBuffer());
        auto editorIndex(static_cast<uint32_t>In_ImGML_Int());
        std::shared_ptr<TextEditor> editorPtr(editorIndex < TextEditors.size() ? TextEditors[editorIndex] : nullptr);
        if (editorPtr == nullptr)
            return -1.0;
        auto& editor(*editorPtr);
        auto title(In_ImGML_String());
        auto x(In_ImGML_Float()); auto y(In_ImGML_Float());
        auto size(ImVec2(x, y));
        auto border(In_ImGML_BoolOpt(false));
        editor.Render(title.c_str(), size, border);
        return 1.0;
    }

    ////
    // void SetText(const std::string& aText);
    IMGML TextEditorSetText(double _id, const char* _text) {
        CheckImGML();
        CGMLTypedBuffer in(imguigml->WrapperBuffer());
        auto editorIndex(static_cast<uint32_t>(_id));
        std::shared_ptr<TextEditor> editorPtr(editorIndex < TextEditors.size() ? TextEditors[editorIndex] : nullptr);
        if (editorPtr == nullptr)
            return -1.0;

        auto& editor(*editorPtr);
        editor.SetText(_text);
        return 1.0;
    }

    ////
    // std::string GetText() const;
    DLL_API const char *TextEditorGetText(double _id ) {
        const auto& imguigml(ImGuiGML::Instance()); 
        auto& io(ImGui::GetIO()); 
        if (imguigml == nullptr)
            return nullptr;
        
        CGMLTypedBuffer in(imguigml->WrapperBuffer());
        auto editorIndex(static_cast<uint32_t>(_id));
        std::shared_ptr<TextEditor> editorPtr(editorIndex < TextEditors.size() ? TextEditors[editorIndex] : nullptr); 
        if (editorPtr == nullptr) 
            return nullptr;
        
        auto& editor(*editorPtr);
		static std::string editorText;
		editorText = editor.GetText();
        return editorText.c_str();
    }

    ////
    // std::string GetSelectedText() const;
    IMGML TextEditorGetSelectedText() {
        CheckImGML_InOut();
        CheckImGML_Text();
        Out_ImGML_String(editor.GetSelectedText());
        return 1.0;
    }

    ////
    // int GetTotalLines() const
    IMGML TextEditorGetTotalLines() {
        CheckImGML_InOut();
        CheckImGML_Text();
        Out_ImGML_Int(editor.GetTotalLines());
        return 1.0;
    }
    
    ////
    // bool IsOverwrite() const  
    IMGML TextEditorIsOverwrite() {
        CheckImGML_InOut();
        CheckImGML_Text();
        Out_ImGML_Bool(editor.IsOverwrite());
        return 1.0;
    }

    ////
    // void SetReadOnly(bool aValue);
    IMGML TextEditorSetReadOnly() {
        CheckImGML_In();
        CheckImGML_Text();
        editor.SetReadOnly(In_ImGML_Bool());
        return 1.0;
    }

    ////
    // bool IsReadOnly() const
    IMGML TextEditorIsReadOnly() {
        CheckImGML_InOut();
        CheckImGML_Text();
        Out_ImGML_Bool(editor.IsReadOnly());
        return 1.0;
    }

    ////
    // Coordinates GetCursorPosition() const
    IMGML TextEditorGetCursorPosition() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto pos(editor.GetCursorPosition());
        Out_ImGML_Int(pos.mLine);
        Out_ImGML_Int(pos.mColumn);
        return 1.0;
    }

    ////
    // void SetCursorPosition(const Coordinates& aPosition);
    IMGML TextEditorSetCursorPosition() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto line(In_ImGML_Int());
        auto column(In_ImGML_Int());
        editor.SetCursorPosition(TextEditor::Coordinates(line, column));
        return 1.0;
    }

    ////
    // void InsertText(const std::string& aValue);
    // void InsertText(const char* aValue);
    IMGML TextEditorInsertText() {
        CheckImGML_InOut();
        CheckImGML_Text();
        editor.InsertText(In_ImGML_String());
        return 1.0;
    }

    ////
    // void MoveUp(int aAmount = 1, bool aSelect = false);
    IMGML TextEditorMoveUp() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto amount(In_ImGML_IntOpt(1));
        auto select(In_ImGML_BoolOpt(false));
        editor.MoveUp(amount, select);
        return 1.0;
    }

    ////
    // void MoveDown(int aAmount = 1, bool aSelect = false);
    IMGML TextEditorMoveDown() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto amount(In_ImGML_IntOpt(1));
        auto select(In_ImGML_BoolOpt(false));
        editor.MoveDown(amount, select);
        return 1.0;
    }

    ////
    // void MoveLeft(int aAmount = 1, bool aSelect = false, bool aWordMode = false);
    IMGML TextEditorMoveLeft() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto amount(In_ImGML_IntOpt(1));
        auto select(In_ImGML_BoolOpt(false));
        editor.MoveLeft(amount, select);
        return 1.0;
    }

    ////
    // void MoveRight(int aAmount = 1, bool aSelect = false, bool aWordMode = false);
    IMGML TextEditorMoveRight() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto amount(In_ImGML_IntOpt(1));
        auto select(In_ImGML_BoolOpt(false));
        editor.MoveRight(amount, select);
        return 1.0;
    }

    ////
    // void MoveTop(bool aSelect = false);
    IMGML TextEditorMoveTop() {
        CheckImGML_InOut();
        CheckImGML_Text();
        editor.MoveTop(In_ImGML_BoolOpt(false));
        return 1.0;
    }
    
    ////
    // void MoveBottom(bool aSelect = false);
    IMGML TextEditorMoveBottom() {
        CheckImGML_InOut();
        CheckImGML_Text();
        editor.MoveBottom(In_ImGML_BoolOpt(false));
        return 1.0;
    }

    ////
    // void MoveHome(bool aSelect = false);
    IMGML TextEditorMoveHome() {
        CheckImGML_InOut();
        CheckImGML_Text();
        editor.MoveHome(In_ImGML_BoolOpt(false));
        return 1.0;
    }

    ////
    // void MoveEnd(bool aSelect = false);
    IMGML TextEditorMoveEnd() {
        CheckImGML_InOut();
        CheckImGML_Text();
        editor.MoveEnd(In_ImGML_BoolOpt(false));
        return 1.0;
    }

    ////
    // void SetSelectionStart(const Coordinates& aPosition);
    IMGML TextEditorSetSelectionStart() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto line(In_ImGML_Int());
        auto column(In_ImGML_Int());
        editor.SetSelectionStart(TextEditor::Coordinates(line, column));
        return 1.0;
    }

    ////
    // void SetSelectionEnd(const Coordinates& aPosition);
    IMGML TextEditorSetSelectionEnd() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto line(In_ImGML_Int());
        auto column(In_ImGML_Int());
        editor.SetSelectionEnd(TextEditor::Coordinates(line, column));
        return 1.0;
    }

    ////
    // void SetSelection(const Coordinates& aStart, const Coordinates& aEnd, bool awordmode = false);
    IMGML TextEditorSetSelection() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto startLine(In_ImGML_Int());
        auto startColumn(In_ImGML_Int());
        auto endLine(In_ImGML_Int());
        auto endColumn(In_ImGML_Int());
        auto wordMode(In_ImGML_BoolOpt(false));
        //TODO: implement word mode enum
		editor.SetSelection(TextEditor::Coordinates(startLine, startColumn), TextEditor::Coordinates(endLine, endColumn), wordMode ? TextEditor::SelectionMode::Word : TextEditor::SelectionMode::Normal);
        return 1.0;
    }

    ////
    // void SelectWordUnderCursor();
    IMGML TextEditorSelectWordUnderCursor() {
        CheckImGML_In();
        CheckImGML_Text();
        editor.SelectWordUnderCursor();
        return 1.0;
    }

    ////
    // bool HasSelection() const;
    IMGML TextEditorHasSelection() {
        CheckImGML_InOut();
        CheckImGML_Text();
        Out_ImGML_Bool(editor.HasSelection());
        return 1.0;
    }

    ////
    // TextEditor::Copy
    IMGML TextEditorCopy() {
        CheckImGML_In();
        CheckImGML_Text();
        editor.Copy();
        return 1.0;
    }

    ////
    // TextEditor::Cut
    IMGML TextEditorCut() {
        CheckImGML_In();
        CheckImGML_Text();
        editor.Cut();
        return 1.0;
    }

    ////
    // TextEditor::Paste
    IMGML TextEditorPaste() {
        CheckImGML_In();
        CheckImGML_Text();
        editor.Paste();
        return 1.0;
    }

    ////
    // TextEditor::Delete
    IMGML TextEditorDelete() {
        CheckImGML_In();
        CheckImGML_Text();
        editor.Delete();
        return 1.0;
    }

    ////
    // bool CanUndo() const;
    IMGML TextEditorCanUndo() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto can(editor.CanUndo());
        Out_ImGML_Bool(can);
        return 1.0;
    }

    ////
    // bool CanRedo() const;
    IMGML TextEditorCanRedo() {
        CheckImGML_InOut();
        CheckImGML_Text();
        auto can(editor.CanRedo());
        Out_ImGML_Bool(can);
        return 1.0;
    }

    ////
    //void Undo(int aSteps = 1);
    IMGML TextEditorUndo() {
        CheckImGML_In();
        CheckImGML_Text();
        auto aSteps(In_ImGML_Int());
        editor.Undo(aSteps);
        return 1.0;
    }

    ////
    //void Redo(int aSteps = 1);
    IMGML TextEditorRedo() {
        CheckImGML_In();
        CheckImGML_Text();
        auto aSteps(In_ImGML_Int());
        editor.Redo(aSteps);
        return 1.0;
    }

    ////
    // static const Palette& GetDarkPalette();
    IMGML TextEditorGetDarkPalette() {
        CheckImGML_InOut();
        const auto& palette(TextEditor::GetDarkPalette());
        for (uint32_t paletteIndex(0); paletteIndex < static_cast<uint32_t>(TextEditor::PaletteIndex::Max); ++paletteIndex)
            Out_ImGML_Ubyte(palette[paletteIndex]);
        return 1.0;
    }

    ////
    // static const Palette& GetLightPalette();
    IMGML TextEditorGetLightPalette() {
        CheckImGML_InOut();
        const auto& palette(TextEditor::GetLightPalette());
        for (uint32_t paletteIndex(0); paletteIndex < static_cast<uint32_t>(TextEditor::PaletteIndex::Max); ++paletteIndex)
            Out_ImGML_Ubyte(palette[paletteIndex]);
        return 1.0;
    }
}

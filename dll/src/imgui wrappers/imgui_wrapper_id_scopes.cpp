#include "imguigml_wrappers.h"

extern "C" {

    //////////////
    // ID scopes
    // If you are creating widgets in a loop you most likely want to push a unique identifier (e.g. object pointer, loop index) so ImGui can differentiate them.
    // You can also use the "##foobar" syntax within widget label to distinguish them from each others. 
    // Read "A primer on the use of labels/IDs" in the FAQ for more details.
    //////////////

    ////
    // IMGUI_API void          PushID(const char* str_id);
    // IMGUI_API void          PushID(const char* str_id_begin, const char* str_id_end);
    // IMGUI_API void          PushID(const void* ptr_id);
    // IMGUI_API void          PushID(int int_id);
    //  push identifier into the ID stack. IDs are hash of the entire stack!
    IMGML PushID() {
        CheckImGML_In();
        if (in.Count() > 1) {
            auto idBegin(In_ImGML_String());
            auto idEnd(In_ImGML_String());
            ImGui::PushID(idBegin.c_str(), idEnd.c_str());
        } else {
            if (in.NextType<std::string>())
                ImGui::PushID(In_ImGML_String().c_str());
            else
                ImGui::PushID(In_ImGML_Int());
            // ImGui::PushId(&(ptr)); // todo: ptr_id support (just use int?)
        }
        return 1.0;
    }

    ////
    // IMGUI_API void          PopID();
    IMGML PopID() {
        CheckImGML();
        ImGui::PopID();
        return 1.0;
    }

    ////
    // IMGUI_API ImGuiID       GetID(const char* str_id);                                          // calculate unique ID (hash of whole ID stack + given parameter). e.g. if you want to query into ImGuiStorage yourself
    // IMGUI_API ImGuiID       GetID(const char* str_id_begin, const char* str_id_end);
    // IMGUI_API ImGuiID       GetID(const void* ptr_id);
    IMGML GetID() {
        CheckImGML_InOut();
        ImGuiID guiID(0);
        if (in.Count() > 1) {
            auto idBegin(In_ImGML_String());
            auto idEnd(In_ImGML_String());
            guiID = ImGui::GetID(idBegin.c_str(), idEnd.c_str());
        }
        else {
            if (in.NextType<std::string>())
                guiID = ImGui::GetID(In_ImGML_String().c_str());
            else // ImGui::GetID(&(ptr)); // just uses uint
                guiID = ImGui::GetID(reinterpret_cast<void*>(static_cast<size_t>(In_ImGML_Int())));
        }
        
        Out_ImGML_Uint(guiID);
        return 1.0;
    }

}
#include "imguigml_wrappers.h"

extern "C" {
    ////
    // IMGUI_API void          BeginTabBar(const char* str_id, ImGuiTabBarFlags flags = 0);
    IMGML BeginTabBar() {
        CheckImGML_In();
        auto strId(In_ImGML_String());
        ImGuiTabBarFlags flags(In_ImGML_IntOpt(0));
        ImGui::BeginTabBar(strId.c_str(), flags);
        return 1.0;
    }

    ////
    // IMGUI_API void          EndTabBar();
    IMGML EndTabBar() {
        CheckImGML();
        ImGui::EndTabBar();
        return 1.0;
    }

    ////
    // IMGUI_API bool          TabItem(const char* label, bool* p_open = NULL, ImGuiTabItemFlags = 0);
    IMGML BeginTabItem() {
        bool open(false);
        bool *popen(nullptr);

        CheckImGML_InOut();
        auto label(In_ImGML_String());
        if (in.NextType<int8_t>()) { open = In_ImGML_Bool(); popen = &open; }
        else In_ImGML_Skip();
        ImGuiTabItemFlags flags(static_cast<ImGuiTabItemFlags>(In_ImGML_IntOpt(0)));

        Out_ImGML_Bool(ImGui::BeginTabItem(label.c_str(), popen, flags));
        if (popen != nullptr)
            Out_ImGML_Bool(open);
        
        return 1.0;
    }

	////
	// ImGui
	IMGML EndTabItem() {
		CheckImGML();
		ImGui::EndTabItem();
		return 1.0;
	}

    ////
    // IMGUI_API void          SetTabItemClosed(const char* label);
    IMGML SetTabItemClosed() {
        CheckImGML_In();
        auto label(In_ImGML_String());
        ImGui::SetTabItemClosed(label.c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetTabItemSelected(const char* label);
    IMGML SetTabItemSelected() {
        CheckImGML_In();
        auto label(In_ImGML_String());
        ImGui::SetTabItemClosed(label.c_str());
        return 1.0;
    }
}
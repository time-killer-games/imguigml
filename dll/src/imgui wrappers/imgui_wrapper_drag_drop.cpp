#include "imguigml_wrappers.h"

extern "C" {

    ////
    // IMGUI_API bool                BeginDragDropSource(ImGuiDragDropFlags flags = 0, int mouse_button = 0);
    //  call when the current item is active. If this return true, you can call SetDragDropPayload() + EndDragDropSource()
    IMGML BeginDragDropSource() {
        CheckImGML_InOut();
        auto flags(In_ImGML_UintOpt(0));
        Out_ImGML_Bool(ImGui::BeginDragDropSource(flags));
        return 1.0;
    }

    ////
    // IMGUI_API bool                SetDragDropPayload(const char* type, const void* data, size_t size, ImGuiCond cond = 0);
    //  type is a user defined string of maximum 8 characters. Strings starting with '_' are reserved for dear imgui internal types. Data is copied and held by imgui.
    IMGML SetDragDropPayload() {
        CheckImGML_InOut();
		auto type(In_ImGML_String());
        auto payloadId(In_ImGML_Uint());
        auto cond(In_ImGML_UintOpt(0));
        Out_ImGML_Bool(ImGui::SetDragDropPayload(type.c_str(), reinterpret_cast<void*>(&payloadId), sizeof(payloadId), cond));
        return 1.0;
    }

    ////
    // IMGUI_API void                EndDragDropSource();
    IMGML EndDragDropSource() {
        CheckImGML();
        ImGui::EndDragDropSource();
        return 1.0;
    }

    ////
    // IMGUI_API bool                BeginDragDropTarget();
    //  call after submitting an item that may receive an item. If this returns true, you can call AcceptDragDropPayload() + EndDragDropTarget()
    IMGML BeginDragDropTarget() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::BeginDragDropTarget());
        return 1.0;
    }

    ////
    // IMGUI_API const ImGuiPayload* AcceptDragDropPayload(const char* type, ImGuiDragDropFlags flags = 0);
    //  accept contents of a given type. If ImGuiDragDropFlags_AcceptBeforeDelivery is set you can peek into the payload before the mouse button is released.
    IMGML AcceptDragDropPayload() {
        CheckImGML_InOut();
		auto type(In_ImGML_String());
        auto flags(In_ImGML_UintOpt(0));

		auto payload(ImGui::AcceptDragDropPayload(type.c_str(), flags));
        Out_ImGML_Bool(payload != nullptr);
        if (payload != nullptr) {
            Out_ImGML_Uint(*reinterpret_cast<const uint32_t*>(payload->Data));
            Out_ImGML_Bool(payload->IsDelivery());
            Out_ImGML_Bool(payload->IsPreview());
        }

        return 1.0;
    }

    ////
    // IMGUI_API void                EndDragDropTarget();
    IMGML EndDragDropTarget() {
        CheckImGML();
        ImGui::EndDragDropTarget();
        return 1.0;
    }

}

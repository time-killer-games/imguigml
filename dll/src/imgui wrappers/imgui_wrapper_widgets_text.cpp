#include "imguigml_wrappers.h"

extern "C" {
    // Widgets: Text
    
    ////
    // IMGUI_API void          TextUnformatted(const char* text, const char* text_end = NULL);               // doesn't require null terminated string if 'text_end' is specified. no copy done, no limits, recommended for long chunks of text
    // IMGUI_API void          Text(const char* fmt, ...)                                     IM_FMTARGS(1); // simple formatted text
    // IMGUI_API void          TextV(const char* fmt, va_list args)                           IM_FMTLIST(1);
    IMGML ImText() {
        CheckImGML_In();
        ImGui::Text("%s", In_ImGML_String().c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          TextColored(const ImVec4& col, const char* fmt, ...)           IM_FMTARGS(2);
    // IMGUI_API void          TextColoredV(const ImVec4& col, const char* fmt, va_list args) IM_FMTLIST(2);
    //  shortcut for PushStyleColor(ImGuiCol_Text, col); Text(fmt, ...); PopStyleColor();
    IMGML ImTextColored() {
        CheckImGML_In();
        float r(In_ImGML_Float());
        float g(In_ImGML_Float());
        float b(In_ImGML_Float());
        float a(In_ImGML_Float());
        ImGui::TextColored(ImVec4(r,g,b,a), "%s", In_ImGML_String().c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          TextDisabled(const char* fmt, ...)                             IM_FMTARGS(1); 
    // IMGUI_API void          TextDisabledV(const char* fmt, va_list args)                   IM_FMTLIST(1);
    //   shortcut for PushStyleColor(ImGuiCol_Text, style.Colors[ImGuiCol_TextDisabled]); Text(fmt, ...); PopStyleColor();
    IMGML ImTextDisabled() {
        CheckImGML_In();
        ImGui::TextDisabled("%s", In_ImGML_String().c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          TextWrapped(const char* fmt, ...)                              IM_FMTARGS(1); 
    // IMGUI_API void          TextWrappedV(const char* fmt, va_list args)                    IM_FMTLIST(1);
    //  shortcut for PushTextWrapPos(0.0f); Text(fmt, ...); PopTextWrapPos();. 
    //  Note that this won't work on an auto-resizing window if there's no other widgets to extend the window width, yoy may need to set a size using SetNextWindowSize().
    IMGML ImTextWrapped() {
        CheckImGML_In();
        ImGui::TextWrapped("%s", In_ImGML_String().c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          LabelText(const char* label, const char* fmt, ...)             IM_FMTARGS(2); 
    // IMGUI_API void          LabelTextV(const char* label, const char* fmt, va_list args)   IM_FMTLIST(2);
    //  display text+label aligned the same way as value+label widgets
    IMGML ImLabelText() {
        CheckImGML_In();
        auto label(In_ImGML_String());
        auto text(In_ImGML_String());
        ImGui::LabelText(label.c_str(), "%s", text.c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          BulletText(const char* fmt, ...)                               IM_FMTARGS(1); 
    // IMGUI_API void          BulletTextV(const char* fmt, va_list args)                     IM_FMTLIST(1);
    //  shortcut for Bullet()+Text()
    IMGML ImBulletText() {
        CheckImGML_In();
        ImGui::BulletText("%s", In_ImGML_String().c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          Bullet();
    //  draw a small circle and keep the cursor on the same line. 
    //  advance cursor x position by GetTreeNodeToLabelSpacing(), same distance that TreeNode() uses
    IMGML ImBullet() {
        CheckImGML();
        ImGui::Bullet();
        return 1.0;
    }
}
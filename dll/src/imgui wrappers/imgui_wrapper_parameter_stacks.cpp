#include "imguigml_wrappers.h"

extern "C" {

    ////////
    // Parameters stacks (shared)
    ////////
    

    ////
    // IMGUI_API void          PushStyleColor(ImGuiCol idx, ImU32 col);
    // IMGUI_API void          PushStyleColor(ImGuiCol idx, const ImVec4& col);
    IMGML PushStyleColor() {
        CheckImGML_In();
        auto idx(In_ImGML_Int());
        if (in.Count() == 2) {
            ImGui::PushStyleColor(idx, In_ImGML_Uint());
        } else {
            float r(In_ImGML_Float());
            float g(In_ImGML_Float());
            float b(In_ImGML_Float());
            float a(In_ImGML_Float());
            ImGui::PushStyleColor(idx, ImVec4(r, g, b, a));
        }
        return 1.0;
    }

    // IMGUI_API void          PopStyleColor(int count = 1);
    IMGML PopStyleColor() {
        CheckImGML_In();
        ImGui::PopStyleColor(In_ImGML_IntOpt(1));
        return 1.0;
    }

    ////
    // IMGUI_API void          PushStyleVar(ImGuiStyleVar idx, float val);
    // IMGUI_API void          PushStyleVar(ImGuiStyleVar idx, const ImVec2& val);
    IMGML PushStyleVar() {
        CheckImGML_In();
        auto idx(In_ImGML_Int());
        auto v(In_ImGML_Float());
        if (in.Count() > 2) {
            ImVec2 val(v, In_ImGML_Float());
            ImGui::PushStyleVar(idx, val);
        } else {
            ImGui::PushStyleVar(idx, v);
        }
        return 1.0;
    }

    ////
    // IMGUI_API void          PopStyleVar(int count = 1);
    IMGML PopStyleVar() {
        CheckImGML_In();
        ImGui::PopStyleVar(In_ImGML_IntOpt(1));
        return 1.0;
    }

    ////
    // IMGUI_API const ImVec4& GetStyleColorVec4(ImGuiCol idx);
    //  retrieve style color as stored in ImGuiStyle structure. use to feed back into PushStyleColor(), otherwhise use GetColorU32() to get style color + style alpha.
    IMGML GetStyleColorVec4() {
        CheckImGML_InOut();
        ImVec4 col(ImGui::GetStyleColorVec4(In_ImGML_Int()));
        Out_ImGML_Float(col.x);
        Out_ImGML_Float(col.y);
        Out_ImGML_Float(col.z);
        Out_ImGML_Float(col.w);
        return 1.0;
    }

    ////
    //IMGUI_API ImU32         GetColorU32(ImGuiCol idx, float alpha_mul = 1.0f);
    // retrieve given style color with style alpha applied and optional extra alpha multiplier
    //IMGUI_API ImU32         GetColorU32(const ImVec4& col);
    // retrieve given color with style alpha applied
    //IMGUI_API ImU32         GetColorU32(ImU32 col);
    // retrieve given color with style alpha applied
    IMGML GetColorU32() {
        CheckImGML_InOut();

        if (in.Count() == 1 && in.NextType<uint32_t>()) {
            Out_ImGML_Uint(ImGui::GetColorU32(In_ImGML_Uint()));
        } else if (in.NextType<int32_t>() && in.Count() <= 2) {
            auto idx(In_ImGML_Int());
            auto alphaMul(In_ImGML_FloatOpt(1.0));
            Out_ImGML_Uint(ImGui::GetColorU32(idx, alphaMul));
        } else if (in.Count() == 4) {
            float r(In_ImGML_Float());
            float g(In_ImGML_Float());
            float b(In_ImGML_Float());
            float a(In_ImGML_Float());
            Out_ImGML_Uint(ImGui::GetColorU32(ImVec4(r, g, b, a)));
        }
        return 1.0;
    }

    ////////////////////
    // Parameters stacks (current window)
    ////////////////////

    ////
    // IMGUI_API void          PushItemWidth(float item_width);
    //  width of items for the common item+label case, pixels. 
    //  0.0f = default to ~2/3 of windows width, >0.0f: width in pixels, <0.0f align xx pixels to the right of window (so -1.0f always align width to the right side)
    IMGML PushItemWidth() {
        CheckImGML_In();
        ImGui::PushItemWidth(In_ImGML_Float());
        return 1.0;
    }

    ////
    // IMGUI_API void          PopItemWidth();
    IMGML PopItemWidth() {
        CheckImGML();
        ImGui::PopItemWidth();
        return 1.0;
    }

    ////
    // IMGUI_API float         CalcItemWidth();
    //  width of item given pushed settings and current cursor position
    IMGML CalcItemWidth() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::CalcItemWidth());
        return 1.0;
    }

    ////
    // IMGUI_API void          PushTextWrapPos(float wrap_pos_x = 0.0f);
    //  word-wrapping for Text*() commands. < 0.0f: no wrapping; 0.0f: wrap to end of window (or column); > 0.0f: wrap at 'wrap_pos_x' position in window local space
    IMGML PushTextWrapPos() {
        CheckImGML_In();
        ImGui::PushTextWrapPos(In_ImGML_FloatOpt(0.0f));
        return 1.0;
    }

    ////
    // IMGUI_API void          PopTextWrapPos();
    IMGML PopTextWrapPos() {
        CheckImGML();
        ImGui::PopTextWrapPos();
        return 1.0;
    }

    ////
    // IMGUI_API void          PushAllowKeyboardFocus(bool allow_keyboard_focus);
    //  allow focusing using TAB/Shift-TAB, enabled by default but you can disable it for certain widgets
    IMGML PushAllowKeyboardFocus() {
        CheckImGML_In();
        ImGui::PushAllowKeyboardFocus(In_ImGML_Bool());
        return 1.0;
    }

    ////
    // IMGUI_API void          PopAllowKeyboardFocus();
    IMGML PopAllowKeyboardFocus() {
        CheckImGML();
        ImGui::PopAllowKeyboardFocus();
        return 1.0;
    }

    ////
    // IMGUI_API void          PushButtonRepeat(bool repeat);
    //  in 'repeat' mode, Button*() functions return repeated true in a typematic manner (using io.KeyRepeatDelay/io.KeyRepeatRate setting). Note that you can call IsItemActive() after any Button() to tell if the button is held in the current frame.
    IMGML PushButtonRepeat() {
        CheckImGML_In();
        ImGui::PushButtonRepeat(In_ImGML_Bool());
        return 1.0;
    }

    ////
    // IMGUI_API void          PopButtonRepeat();
    IMGML PopButtonRepeat() {
        CheckImGML();
        ImGui::PopButtonRepeat();
        return 1.0;
    }

}
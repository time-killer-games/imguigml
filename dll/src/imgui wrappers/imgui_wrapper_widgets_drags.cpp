#include "imguigml_wrappers.h"

extern "C" {

    ///////////
    // Widgets: Drags (tip: ctrl+click on a drag box to input with keyboard. manually input values aren't clamped, can go off-bounds)
    // For all the Float2/Float3/Float4/Int2/Int3/Int4 versions of every functions, note that a 'float v[X]' function argument is the same as 'float* v', the array syntax is just a way to document the number of elements that are expected to be accessible. You can pass address of your first element out of a contiguous set, e.g. &myvector.x
    ///////////

    ////
    // IMGUI_API bool          DragFloat(const char* label, float* v, float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    //  If v_min >= v_max we have no bound
    IMGML DragFloat() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        float v(In_ImGML_Float());
        float v_speed(In_ImGML_FloatOpt(1.0f));
        float v_min(In_ImGML_FloatOpt(0.0f));
        float v_max(In_ImGML_FloatOpt(0.0f));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::DragFloat(label.c_str(), &v, v_speed, v_min, v_max, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v);
        return 1.0;
    }

    ////
    // IMGUI_API bool          DragFloat2(const char* label, float v[2], float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    IMGML DragFloat2() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        float v[2] = { 0.0f, 0.0f };
        v[0] = In_ImGML_Float();
        v[1] = In_ImGML_Float();
        float v_speed(In_ImGML_FloatOpt(1.0f));
        float v_min(In_ImGML_FloatOpt(0.0f));
        float v_max(In_ImGML_FloatOpt(0.0f));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::DragFloat2(label.c_str(), v, v_speed, v_min, v_max, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v[0]);
        Out_ImGML_Float(v[1]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          DragFloat3(const char* label, float v[3], float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    IMGML DragFloat3() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        float v[3] = { 0.0f, 0.0f, 0.0f };
        v[0] = In_ImGML_Float();
        v[1] = In_ImGML_Float();
        v[2] = In_ImGML_Float();
        float v_speed(In_ImGML_FloatOpt(1.0f));
        float v_min(In_ImGML_FloatOpt(0.0f));
        float v_max(In_ImGML_FloatOpt(0.0f));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::DragFloat3(label.c_str(), v, v_speed, v_min, v_max, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v[0]);
        Out_ImGML_Float(v[1]);
        Out_ImGML_Float(v[2]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          DragFloat4(const char* label, float v[4], float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    IMGML DragFloat4() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        float v[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
        v[0] = In_ImGML_Float();
        v[1] = In_ImGML_Float();
        v[2] = In_ImGML_Float();
        v[3] = In_ImGML_Float();
        float v_speed(In_ImGML_FloatOpt(1.0f));
        float v_min(In_ImGML_FloatOpt(0.0f));
        float v_max(In_ImGML_FloatOpt(0.0f));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));
        bool ret(ImGui::DragFloat4(label.c_str(), v, v_speed, v_min, v_max, displayFmt.c_str(), flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v[0]);
        Out_ImGML_Float(v[1]);
        Out_ImGML_Float(v[2]);
        Out_ImGML_Float(v[3]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          DragFloatRange2(const char* label, float* v_current_min, float* v_current_max, float v_speed = 1.0f, float v_min = 0.0f, float v_max = 0.0f, const char* display_format = "%.3f", const char* display_format_max = NULL, ImGuiSliderFlags flags = ImGuiSlideFlags_None);
    IMGML DragFloatRange2() {
        CheckImGML_InOut();
        std::string strDisplayFmtMax;
        auto label(In_ImGML_String());
        float v_cur_min(In_ImGML_Float());
        float v_cur_max(In_ImGML_Float());
        float v_speed(In_ImGML_FloatOpt(1.0f));
        float v_min(In_ImGML_FloatOpt(0.0f));
        float v_max(In_ImGML_FloatOpt(0.0f));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        const char* displayFmtMax(nullptr);
        if (in.Count() > 7 && in.IsType<std::string>(7)) {
            strDisplayFmtMax = In_ImGML_String();
            displayFmtMax = strDisplayFmtMax.c_str();
        } else 
            In_ImGML_BoolOpt(false);
        ImGuiSliderFlags flags(In_ImGML_IntOpt(ImGuiSliderFlags_None));

        bool ret(ImGui::DragFloatRange2(label.c_str(), &v_cur_min, &v_cur_max, v_speed, v_min, v_max, displayFmt.c_str(), displayFmtMax, flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(v_cur_min);
        Out_ImGML_Float(v_cur_max);
        return 1.0;

    }

    ////
    // IMGUI_API bool          DragInt(const char* label, int* v, float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f");                                       // If v_min >= v_max we have no bound
    IMGML DragInt() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        int v(In_ImGML_Int());
        float v_speed(In_ImGML_FloatOpt(1.0f));
        int v_min(In_ImGML_IntOpt(0));
        int v_max(In_ImGML_IntOpt(0));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));

        bool ret(ImGui::DragInt(label.c_str(), &v, v_speed, v_min, v_max, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v);
        
        return 1.0;
    }

    ////
    // IMGUI_API bool          DragInt2(const char* label, int v[2], float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f");
    IMGML DragInt2() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        int v[2] = { 0, 0, };
        v[0] = In_ImGML_Int();
        v[1] = In_ImGML_Int();
        float v_speed(In_ImGML_FloatOpt(1.0f));
        int v_min(In_ImGML_IntOpt(0));
        int v_max(In_ImGML_IntOpt(0));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));

        bool ret(ImGui::DragInt2(label.c_str(), v, v_speed, v_min, v_max, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v[0]);
        Out_ImGML_Int(v[1]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          DragInt3(const char* label, int v[3], float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f");
    IMGML DragInt3() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        int v[3] = { 0, 0, 0 };
        v[0] = In_ImGML_Int();
        v[1] = In_ImGML_Int();
        v[2] = In_ImGML_Int();
        float v_speed(In_ImGML_FloatOpt(1.0f));
        int v_min(In_ImGML_IntOpt(0));
        int v_max(In_ImGML_IntOpt(0));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));

        bool ret(ImGui::DragInt3(label.c_str(), v, v_speed, v_min, v_max, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v[0]);
        Out_ImGML_Int(v[1]);
        Out_ImGML_Int(v[2]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          DragInt4(const char* label, int v[4], float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f");
    IMGML DragInt4() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        int v[4] = { 0, 0, 0, 0 };
        v[0] = In_ImGML_Int();
        v[1] = In_ImGML_Int();
        v[2] = In_ImGML_Int();
        v[3] = In_ImGML_Int();
        float v_speed(In_ImGML_FloatOpt(1.0f));
        int v_min(In_ImGML_IntOpt(0));
        int v_max(In_ImGML_IntOpt(0));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        
        bool ret(ImGui::DragInt4(label.c_str(), v, v_speed, v_min, v_max, displayFmt.c_str()));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v[0]);
        Out_ImGML_Int(v[1]);
        Out_ImGML_Int(v[2]);
        Out_ImGML_Int(v[3]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          DragIntRange2(const char* label, int* v_current_min, int* v_current_max, float v_speed = 1.0f, int v_min = 0, int v_max = 0, const char* display_format = "%.0f", const char* display_format_max = NULL);
    IMGML DragIntRange2() {
        CheckImGML_InOut();
        std::string strDisplayFmtMax;
        auto label(In_ImGML_String());
        int v_cur_min(In_ImGML_Int());
        int v_cur_max(In_ImGML_Int());
        float v_speed(In_ImGML_FloatOpt(1.0f));
        int v_min(In_ImGML_IntOpt(0));
        int v_max(In_ImGML_IntOpt(0));
        std::string displayFmt(In_ImGML_StringOpt("%.3f"));
        const char* displayFmtMax(nullptr);
        if (in.Count() > 7 && in.IsType<std::string>(7)) {
            strDisplayFmtMax = In_ImGML_String();
            displayFmtMax = strDisplayFmtMax.c_str();
        }
        else
            In_ImGML_BoolOpt(false);
        
        bool ret(ImGui::DragIntRange2(label.c_str(), &v_cur_min, &v_cur_max, v_speed, v_min, v_max, displayFmt.c_str(), displayFmtMax));
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(v_cur_min);
        Out_ImGML_Int(v_cur_max);
        return 1.0;
    }

}
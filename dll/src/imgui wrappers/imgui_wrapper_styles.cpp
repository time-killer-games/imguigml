#include "imguigml_wrappers.h"

extern "C" {
    ////
    // IMGUI_API void          ShowStyleEditor(ImGuiStyle* ref = NULL);
    //  add style editor block (not a window). you can pass in a reference ImGuiStyle structure to compare to, revert to and save to (else it uses the default style)
    IMGML ShowStyleEditor() {
        CheckImGML_In();
        ImGuiStyle* ref(nullptr);
        //todo: ImGuiStyle* ref(imguigml->GetStyle(_styleIndex));
        ImGui::ShowStyleEditor(ref);
        return 1.0;
    }

    ////
    // IMGUI_API void ShowStyleSelector(const char* label)
    IMGML ShowStyleSelector() {
        CheckImGML_In();
		auto label(In_ImGML_String());
        ImGui::ShowStyleSelector(label.c_str());
        return 1.0;
    }

    // Styles
    ////
    // IMGUI_API void          StyleColorsClassic(ImGuiStyle* dst = NULL);
    IMGML StyleColorsClassic() {
        CheckImGML_In();
        
        ImGuiStyle* ref(nullptr);
        //todo: ImGuiStyle* ref(imguigml->GetStyle(_styleIndex));
        ImGui::StyleColorsClassic(ref);
        
        return 1.0;
    }

    ////
    // IMGUI_API void          StyleColorsDark(ImGuiStyle* dst = NULL);
    IMGML StyleColorsDark() {
        CheckImGML_In();
        
        ImGuiStyle* ref(nullptr);
        //todo: ImGuiStyle* ref(imguigml->GetStyle(_styleIndex));
        ImGui::StyleColorsDark(ref);

        return 1.0;
    }

    ////
    // IMGUI_API void          StyleColorsLight(ImGuiStyle* dst = NULL);
    IMGML StyleColorsLight() {
        CheckImGML_In();

        ImGuiStyle* ref(nullptr);
        //todo: ImGuiStyle* ref(imguigml->GetStyle(_styleIndex));
        ImGui::StyleColorsLight(ref);

        return 1.0;
    }
}

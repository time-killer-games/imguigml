#include "imguigml_wrappers.h"

extern "C" {
    ////
    // Internal context access - if you want to use multiple context, share context between modules (e.g. DLL). There is a default context created and active by default.
    // All contexts share a same ImFontAtlas by default. If you want different font atlas, you can new() them and overwrite the GetIO().Fonts variable of an ImGui context.
    ////

    ////
    // IMGUI_API const char*   GetVersion();
    IMGML ImGetVersion() {
        CheckImGML_Out();
        Out_ImGML_String(ImGui::GetVersion());
        return 1.0;
    }

    ////
    // IMGUI_API ImGuiContext* CreateContext(void* (*malloc_fn)(size_t) = NULL, void(*free_fn)(void*) = NULL);
    IMGML ImCreateContext() {
        ImGML_NotImplemented();
    }

    ////
    // IMGUI_API void          DestroyContext(ImGuiContext* ctx);
    IMGML ImDestroyContext() {
        ImGML_NotImplemented();
    }

    ////
    // IMGUI_API ImGuiContext* GetCurrentContext();
    IMGML ImGetCurrentContext() {
        ImGML_NotImplemented();
    }

    ////
    // IMGUI_API void          SetCurrentContext(ImGuiContext* ctx);
    IMGML ImSetCurrentContext() {
        ImGML_NotImplemented();
    }

}
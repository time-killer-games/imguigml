#include "imguigml_wrappers.h"

extern "C" {
    ////////////////
    // Widgets: Main
    ////////////////

    ////
    // IMGUI_API bool          Button(const char* label, const ImVec2& size = ImVec2(0, 0));
    //   button
    IMGML Button() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        auto x(In_ImGML_FloatOpt(0.0f));
        auto y(In_ImGML_FloatOpt(0.0f));
        Out_ImGML_Bool(ImGui::Button(label.c_str(), ImVec2(x, y)));
        return 1.0;
    }

    ////
    // IMGUI_API bool          SmallButton(const char* label);
    //  button with FramePadding=(0,0) to easily embed within text
    IMGML SmallButton() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::SmallButton(In_ImGML_String().c_str()));
        return 1.0;
    }
    
    ////
    // IMGUI_API bool          InvisibleButton(const char* str_id, const ImVec2& size);
    IMGML InvisibleButton() {
        CheckImGML_InOut();
        auto strId(In_ImGML_String());
        auto x(In_ImGML_Float());
        auto y(In_ImGML_Float());
        Out_ImGML_Bool(ImGui::InvisibleButton(strId.c_str(), ImVec2(x, y)));
        return 1.0;
    }

    ////
    // IMGUI_API void          Image(ImTextureID user_texture_id, const ImVec2& size, const ImVec2& uv0 = ImVec2(0, 0), const ImVec2& uv1 = ImVec2(1, 1), const ImVec4& tint_col = ImVec4(1, 1, 1, 1), const ImVec4& border_col = ImVec4(0, 0, 0, 0));
    IMGML Image() {
        CheckImGML_In();
        
        // todo: make texture ids means something between these two guys
        auto texId(In_ImGML_Uint());
        auto x (In_ImGML_Float());
        auto y (In_ImGML_Float());
        auto u0(In_ImGML_FloatOpt(0.0f));
        auto v0(In_ImGML_FloatOpt(0.0f));
        auto u1(In_ImGML_FloatOpt(1.0f));
        auto v1(In_ImGML_FloatOpt(1.0f));
        auto tint_r(In_ImGML_FloatOpt(1.0f));
        auto tint_g(In_ImGML_FloatOpt(1.0f));
        auto tint_b(In_ImGML_FloatOpt(1.0f));
        auto tint_a(In_ImGML_FloatOpt(1.0f));
        auto border_r(In_ImGML_FloatOpt(0.0f));
        auto border_g(In_ImGML_FloatOpt(0.0f));
        auto border_b(In_ImGML_FloatOpt(0.0f));
        auto border_a(In_ImGML_FloatOpt(0.0f));
        
        ImVec2 size(x, y),
               uv0(u0, v0),
               uv1(u1, v1);
        ImVec4 tint(tint_r, tint_g, tint_b, tint_a),
               border(border_r, border_g, border_b, border_a);

        ImGui::Image(reinterpret_cast<void*>(static_cast<size_t>(texId)), size, uv0, uv1, tint, border);
        return 1.0;
    }

    ////
    // IMGUI_API bool          ImageButton(ImTextureID user_texture_id, const ImVec2& size, const ImVec2& uv0 = ImVec2(0, 0), const ImVec2& uv1 = ImVec2(1, 1), int frame_padding = -1, const ImVec4& bg_col = ImVec4(0, 0, 0, 0), const ImVec4& tint_col = ImVec4(1, 1, 1, 1));
    //  <0 frame_padding uses default frame padding settings. 0 for no padding
    IMGML ImageButton() {
        CheckImGML_InOut();

        // todo: make texture ids means something between these two guys
        auto texId(In_ImGML_Uint());
        auto x(In_ImGML_Float());
        auto y(In_ImGML_Float());
        auto u0(In_ImGML_FloatOpt(0.0f));
        auto v0(In_ImGML_FloatOpt(0.0f));
        auto u1(In_ImGML_FloatOpt(1.0f));
        auto v1(In_ImGML_FloatOpt(1.0f));
        auto framePadding(In_ImGML_IntOpt(-1));
        auto bg_r(In_ImGML_FloatOpt(0.0f));
        auto bg_g(In_ImGML_FloatOpt(0.0f));
        auto bg_b(In_ImGML_FloatOpt(0.0f));
        auto bg_a(In_ImGML_FloatOpt(0.0f));
        auto tint_r(In_ImGML_FloatOpt(1.0f));
        auto tint_g(In_ImGML_FloatOpt(1.0f));
        auto tint_b(In_ImGML_FloatOpt(1.0f));
        auto tint_a(In_ImGML_FloatOpt(1.0f));

        ImVec2 size(x, y),
               uv0(u0, v0),
               uv1(u1, v1);

        ImVec4 tint(tint_r, tint_g, tint_b, tint_a),
               bg(bg_r, bg_g, bg_b, bg_a);

        Out_ImGML_Bool(ImGui::ImageButton(reinterpret_cast<void*>(static_cast<size_t>(texId)), size, uv0, uv1, framePadding, bg, tint));
        return 1.0;
    }

    ////
    //IMGUI_API bool          Checkbox(const char* label, bool* v);
    IMGML Checkbox() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        bool checked(In_ImGML_Bool());
        Out_ImGML_Bool(ImGui::Checkbox(label.c_str(), &checked));
        Out_ImGML_Bool(checked);
        return 1.0;
    }

    ////
    // IMGUI_API bool          CheckboxFlags(const char* label, unsigned int* flags, unsigned int flags_value);
    IMGML CheckboxFlags() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        uint32_t flags(In_ImGML_Uint());
        uint32_t flagsVal(In_ImGML_Uint());
        
        Out_ImGML_Bool(ImGui::CheckboxFlags(label.c_str(), &flags, flagsVal));
        Out_ImGML_Uint(flags);
        return 1.0;
    }

    ////
    // IMGUI_API bool          RadioButton(const char* label, bool active);
    // IMGUI_API bool          RadioButton(const char* label, int* v, int v_button);
    IMGML RadioButton() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());

        if (in.Count() == 2) {
            bool active(In_ImGML_Bool());
            Out_ImGML_Bool(ImGui::RadioButton(label.c_str(), active));
        } else {
            auto v(In_ImGML_Int());
            auto vb(In_ImGML_Int());
            Out_ImGML_Bool(ImGui::RadioButton(label.c_str(), &v, vb));
            Out_ImGML_Int(v);
        }
         
        return 1.0;
    }

    ////
    // IMGUI_API void          PlotLines(const char* label, const float* values, int values_count, int values_offset = 0, const char* overlay_text = NULL, float scale_min = FLT_MAX, float scale_max = FLT_MAX, ImVec2 graph_size = ImVec2(0, 0), int stride = sizeof(float));
    IMGML PlotLines() {
        std::vector<float> values;
        std::string strOverlay;
        float scaleMin(FLT_MAX);
        float scaleMax(FLT_MAX);
        const char* overlayText(nullptr);
        
        CheckImGML_In();
        auto label(In_ImGML_String());
        auto valueCount(In_ImGML_Int());
        for (int i(0); i < valueCount; ++i) values.push_back(In_ImGML_Float());
        auto valueOffset(In_ImGML_Int());
        
        if (in.NextType<std::string>()) { strOverlay = In_ImGML_String(); overlayText = strOverlay.c_str(); }
        else In_ImGML_Skip();
        
        if (in.NextType<float>()) scaleMin = In_ImGML_Float(); else In_ImGML_Skip();
        if (in.NextType<float>()) scaleMax = In_ImGML_Float(); else In_ImGML_Skip();
        
        float x(In_ImGML_FloatOpt(0.0f));
        float y(In_ImGML_FloatOpt(0.0f));
        int stride(In_ImGML_IntOpt(4));

        ImGui::PlotLines(label.c_str(), values.data(), valueCount, valueOffset, overlayText, scaleMin, scaleMax, ImVec2(x, y), stride);
        return 1.0;
    }

    ////
    // IMGUI_API void          PlotHistogram(const char* label, const float* values, int values_count, int values_offset = 0, const char* overlay_text = NULL, float scale_min = FLT_MAX, float scale_max = FLT_MAX, ImVec2 graph_size = ImVec2(0, 0), int stride = sizeof(float));
    IMGML PlotHistogram() {
        std::vector<float> values;
        std::string strOverlay;
        float scaleMin(FLT_MAX);
        float scaleMax(FLT_MAX);
        const char* overlayText(nullptr);

        CheckImGML_In();
        auto label(In_ImGML_String());
        auto valueCount(In_ImGML_Int());
        for (int i(0); i < valueCount; ++i) values.push_back(In_ImGML_Float());
        auto valueOffset(In_ImGML_Int());

        if (in.NextType<std::string>()) { strOverlay = In_ImGML_String(); overlayText = strOverlay.c_str(); }
        else In_ImGML_Skip();

        if (in.NextType<float>()) scaleMin = In_ImGML_Float(); else In_ImGML_Skip();
        if (in.NextType<float>()) scaleMax = In_ImGML_Float(); else In_ImGML_Skip();

        float x(In_ImGML_FloatOpt(0.0f));
        float y(In_ImGML_FloatOpt(0.0f));
        int stride(In_ImGML_IntOpt(4));

        ImGui::PlotHistogram(label.c_str(), values.data(), valueCount, valueOffset, overlayText, scaleMin, scaleMax, ImVec2(x, y), stride);
        return 1.0;
    }

    ////
    // IMGUI_API void          ProgressBar(float fraction, const ImVec2& size_arg = ImVec2(-1, 0), const char* overlay = NULL);
    IMGML ProgressBar() {
        CheckImGML_In();
        float fraction(In_ImGML_Float());
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());
        const char* overlay(nullptr);
        std::string strOverlay;
        if (in.Count() > 3 && in.IsType<std::string>(3)) {
            strOverlay = In_ImGML_String();
            overlay = strOverlay.c_str();
        }

        ImGui::ProgressBar(fraction, ImVec2(x, y), overlay);
        return 1.0;
    }

}
#include "imguigml_wrappers.h"

namespace {
    std::unordered_set<std::string> StringSet;
}

const char* RetainString(const std::string& _str) {
    auto itFind(StringSet.find(_str));
    if (itFind == StringSet.end()) {
        auto itStr(StringSet.insert(_str));
        return (*itStr.first).c_str();
    }
    return (*itFind).c_str();
}

extern "C" {
    /////////
    // Inputs
    /////////

    ////
    // IMGUI_API int           GetKeyIndex(ImGuiKey imgui_key);
    //  map ImGuiKey_* values into user's key index. == io.KeyMap[key]
    IMGML ImGetKeyIndex() {
        CheckImGML_InOut();
        Out_ImGML_Int(ImGui::GetKeyIndex(In_ImGML_Int()));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsKeyDown(int user_key_index);
    //  is key being held. == io.KeysDown[user_key_index]. note that imgui doesn't know the semantic of each entry of io.KeyDown[]. Use your own indices/enums according to how your backend/engine stored them into KeyDown[]!
    IMGML ImIsKeyDown() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsKeyDown(In_ImGML_Int()));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsKeyPressed(int user_key_index, bool repeat = true);
    //  was key pressed (went from !Down to Down). if repeat=true, uses io.KeyRepeatDelay / KeyRepeatRate
    IMGML ImIsKeyPressed() {
        CheckImGML_InOut();
        int32_t keyIndex(In_ImGML_Int());
        bool repeat(In_ImGML_Bool());
        Out_ImGML_Bool(ImGui::IsKeyPressed(keyIndex, repeat));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsKeyReleased(int user_key_index);
    //  was key released (went from Down to !Down)..
    IMGML ImIsKeyReleased() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsKeyReleased(In_ImGML_Int()));
        return 1.0;
    }

    ////
    // IMGUI_API int           GetKeyPressedAmount(int key_index, float repeat_delay, float rate);
    //  uses provided repeat rate/delay. return a count, most often 0 or 1 but might be >1 if RepeatRate is small enough that DeltaTime > RepeatRate
    IMGML ImGetKeyPressedAmount() {
        CheckImGML_InOut();
        auto keyIndex(In_ImGML_Int());
        auto repeatDelay(In_ImGML_Float());
        auto rate(In_ImGML_Float());
        Out_ImGML_Int(ImGui::GetKeyPressedAmount(keyIndex, repeatDelay, rate));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsMouseDown(int button);
    //  is mouse button held
    IMGML ImIsMouseDown() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsMouseDown(In_ImGML_Int()));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsMouseClicked(int button, bool repeat = false); 
    //  did mouse button clicked (went from !Down to Down)
    IMGML ImIsMouseClicked() {
        CheckImGML_InOut();
        auto button(In_ImGML_Int());
        auto repeat(In_ImGML_BoolOpt(false));
        Out_ImGML_Bool(ImGui::IsMouseClicked(button, repeat));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsMouseDoubleClicked(int button);         
    //  did mouse button double-clicked. a double-click returns false in IsMouseClicked(). uses io.MouseDoubleClickTime.
    IMGML ImIsMouseDoubleClicked() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsMouseDoubleClicked(In_ImGML_Int()));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsMouseReleased(int button);   
    //  did mouse button released (went from Down to !Down)
    IMGML ImIsMouseReleased() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsMouseReleased(In_ImGML_Int()));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsMouseDragging(int button = 0, float lock_threshold = -1.0f); 
    //  is mouse dragging. if lock_threshold < -1.0f uses io.MouseDraggingThreshold
    IMGML ImIsMouseDragging() {
        CheckImGML_InOut();
        auto button(In_ImGML_IntOpt(0));
        auto lockThreshold(In_ImGML_FloatOpt(-1.0f));
        Out_ImGML_Bool(ImGui::IsMouseDragging(button, lockThreshold));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsMouseHoveringRect(const ImVec2& r_min, const ImVec2& r_max, bool clip = true);
    //  is mouse hovering given bounding rect (in screen space). clipped by current clipping settings. disregarding of consideration of focus/window ordering/blocked by a popup.
    IMGML ImIsMouseHoveringRect() {
        CheckImGML_InOut();
        auto minX(In_ImGML_Float());
        auto minY(In_ImGML_Float());
        auto maxX(In_ImGML_Float());
        auto maxY(In_ImGML_Float());
        auto clip(In_ImGML_BoolOpt(true));
        Out_ImGML_Bool(ImGui::IsMouseHoveringRect(ImVec2(minX, minY), ImVec2(maxX, maxY), clip));
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsMousePosValid(const ImVec2* mouse_pos = NULL);  
    IMGML ImIsMousePosValid() {
        CheckImGML_Out();
        // doesn't pass the pointer, just use GetMousePos?
        Out_ImGML_Bool(ImGui::IsMousePosValid());
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetMousePos();                                             
    //  shortcut to ImGui::GetIO().MousePos provided by user, to be consistent with other calls
    IMGML ImGetMousePos() {
        CheckImGML_Out();
        auto pos(ImGui::GetMousePos());
        Out_ImGML_Float(pos.x);
        Out_ImGML_Float(pos.y);
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetMousePosOnOpeningCurrentPopup();            
    //  retrieve backup of mouse positioning at the time of opening popup we have BeginPopup() into
    IMGML ImGetMousePosOnOpeningCurrentPopup() {
        CheckImGML_Out();
        auto pos(ImGui::GetMousePosOnOpeningCurrentPopup());
        Out_ImGML_Float(pos.x);
        Out_ImGML_Float(pos.y);
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetMouseDragDelta(int button = 0, float lock_threshold = -1.0f); 
    //  dragging amount since clicking. if lock_threshold < -1.0f uses io.MouseDraggingThreshold
    IMGML ImGetMouseDragDelta() {
        CheckImGML_InOut();
        auto button(In_ImGML_IntOpt(0));
        auto lockThreshold(In_ImGML_FloatOpt(-1.0f));
        auto delta(ImGui::GetMouseDragDelta(button, lockThreshold));
        Out_ImGML_Float(delta.x);
        Out_ImGML_Float(delta.y);
        return 1.0;
    }

    ////
    // IMGUI_API void          ResetMouseDragDelta(int button = 0);               
    IMGML ImResetMouseDragDelta() {
        CheckImGML_In();
        ImGui::ResetMouseDragDelta(In_ImGML_IntOpt(0));
        return 1.0;
    }

    ////
    // IMGUI_API ImGuiMouseCursor GetMouseCursor();                        
    //  get desired cursor type, reset in ImGui::NewFrame(), this is updated during the frame. valid before Render(). If you use software rendering by setting io.MouseDrawCursor ImGui will render those for you
    IMGML ImGetMouseCursor() {
        CheckImGML_Out();
        Out_ImGML_Int(ImGui::GetMouseCursor());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetMouseCursor(ImGuiMouseCursor type);        
    //  set desired cursor type
    IMGML ImSetMouseCursor() {
        CheckImGML_In();
        ImGui::SetMouseCursor(In_ImGML_Int());
        return 1.0;
    }

    ////
    // IMGUI_API void          CaptureKeyboardFromApp(bool capture = true);       
    //  manually override io.WantCaptureKeyboard flag next frame (said flag is entirely left for your application handle). e.g. force capture keyboard when your widget is being hovered.
    IMGML ImCaptureKeyboardFromApp() {
        CheckImGML_In();
        ImGui::CaptureKeyboardFromApp(In_ImGML_BoolOpt(true));
        return 1.0;
    }

    ////
    // IMGUI_API void          CaptureMouseFromApp(bool capture = true);     
    //  manually override io.WantCaptureMouse flag next frame (said flag is entirely left for your application handle).
    IMGML ImCaptureMouseFromApp() {
        CheckImGML_In();
        ImGui::CaptureMouseFromApp(In_ImGML_BoolOpt(true));
        return 1.0;
    }

    //////////////
    // Helpers functions to access functions pointers in ImGui::GetIO()
    //////////////
    
    ////
    // IMGUI_API const char*   GetClipboardText();
    IMGML ImGetClipboardText() {
        CheckImGML_Out();
        Out_ImGML_String(ImGui::GetClipboardText());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetClipboardText(const char* text);
    IMGML ImSetClipboardText() {
        CheckImGML_In();
        ImGui::SetClipboardText(In_ImGML_String().c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void*         MemAlloc(size_t sz);
    IMGML ImMemAlloc() {
        CheckImGML();
        //ImGui::MemAlloc(_sz);
        ImGML_NotImplemented();
    }

    ////
    // IMGUI_API void          MemFree(void* ptr);
    IMGML ImMemFree() {
        CheckImGML();
        //ImGui::MemFree(ptr);
        ImGML_NotImplemented();
    }
    
#define IOSettingString( settingName )        
#define IOSettingVec2( settingName ) 

    // = 5.0f               // Maximum time between saving positions/sizes to .ini file, in seconds.
    IMGML IOGetIniSavingRate() { CheckImGML_Out(); out.Write<float>(io.IniSavingRate); return 1.0; } 
    IMGML IOSetIniSavingRate() { CheckImGML_In(); io.IniSavingRate = in.NextVal<float>(); return 1.0; }
    
    // = 0.30f              // Time for a double-click, in seconds.
    IMGML IOGetMouseDoubleClickTime() { CheckImGML_Out(); out.Write<float>(io.MouseDoubleClickTime); return 1.0; } 
    IMGML IOSetMouseDoubleClickTime() { CheckImGML_In(); io.MouseDoubleClickTime = in.NextVal<float>(); return 1.0; }
    
    // = 6.0f               // Distance threshold to stay in to validate a double-click, in pixels.
    IMGML IOGetMouseDoubleClickMaxDist() { CheckImGML_Out(); out.Write<float>(io.MouseDoubleClickMaxDist); return 1.0; } 
    IMGML IOSetMouseDoubleClickMaxDist() { CheckImGML_In(); io.MouseDoubleClickMaxDist = in.NextVal<float>(); return 1.0; }
    
    // = 6.0f               // Distance threshold before considering we are dragging
    IMGML IOGetMouseDragThreshold() { CheckImGML_Out(); out.Write<float>(io.MouseDragThreshold); return 1.0; } 
    IMGML IOSetMouseDragThreshold() { CheckImGML_In(); io.MouseDragThreshold = in.NextVal<float>(); return 1.0; }

    // = 0.250f             // When holding a key/button, time before it starts repeating, in seconds (for buttons in Repeat mode, etc.).
    IMGML IOGetKeyRepeatDelay() { CheckImGML_Out(); out.Write<float>(io.KeyRepeatDelay); return 1.0; } 
    IMGML IOSetKeyRepeatDelay() { CheckImGML_In(); io.KeyRepeatDelay = in.NextVal<float>(); return 1.0; }

    // = 0.050f             // When holding a key/button, rate at which it repeats, in seconds.
    IMGML IOGetKeyRepeatRate() { CheckImGML_Out(); out.Write<float>(io.KeyRepeatRate); return 1.0; } 
    IMGML IOSetKeyRepeatRate() { CheckImGML_In(); io.KeyRepeatRate = in.NextVal<float>(); return 1.0; }
    
    // = 1.0f               // Global scale all fonts
    IMGML IOGetFontGlobalScale() { CheckImGML_Out(); out.Write<float>(io.FontGlobalScale); return 1.0; } 
    IMGML IOSetFontGlobalScale() { CheckImGML_In(); io.FontGlobalScale = in.NextVal<float>(); return 1.0; }
    
    // = false              // Allow user scaling text of individual window with CTRL+Wheel.
    IMGML IOGetFontAllowUserScaling() { CheckImGML_Out(); Out_ImGML_Bool(io.FontAllowUserScaling); return 1.0; }
    IMGML IOSetFontAllowUserScaling() { CheckImGML_In(); io.FontAllowUserScaling = In_ImGML_Bool(); return 1.0; }
    
    // = (1.0f,1.0f)        // For retina display or other situations where window coordinates are different from framebuffer coordinates. User storage only, presently not used by ImGui.
    IMGML IOGetDisplayFramebufferScale() { CheckImGML_Out(); Out_ImGML_Float(io.DisplayFramebufferScale.x); Out_ImGML_Float(io.DisplayFramebufferScale.y); return 1.0; } 
    IMGML IOSetDisplayFramebufferScale() { CheckImGML_In(); io.DisplayFramebufferScale.x = In_ImGML_Float(); io.DisplayFramebufferScale.x = In_ImGML_Float(); return 1.0; }
    
    // = defined(__APPLE__) // OS X style: Text editing cursor movement using Alt instead of Ctrl, Shortcuts using Cmd/Super instead of Ctrl, Line/Text Start and End using Cmd+Arrows instead of Home/End, Double click selects by word instead of selecting whole text, Multi-selection in lists uses Cmd/Super instead of Ctrl
    IMGML IOGetOptMacOSXBehaviors() { CheckImGML_Out(); Out_ImGML_Bool(io.ConfigMacOSXBehaviors); return 1.0; }
    IMGML IOSetOptMacOSXBehaviors() { CheckImGML_In(); io.ConfigMacOSXBehaviors = In_ImGML_Bool(); return 1.0; }

    // = "imgui.ini"        // Path to .ini file. NULL to disable .ini saving.
    IMGML IOGetIniFilename() { CheckImGML_Out(); Out_ImGML_String(io.IniFilename); return 1.0; }
    IMGML IOSetIniFilename() { CheckImGML_In(); io.IniFilename = RetainString(In_ImGML_String()); return 1.0; }

    // = "imgui_log.txt"    // Path to .log file (default parameter to ImGui::LogToFile when no file is specified).
    IMGML IOGetLogFilename() { CheckImGML_Out(); Out_ImGML_String(io.LogFilename); return 1.0; }
    IMGML IOSetLogFilename() { CheckImGML_In(); io.LogFilename = RetainString(In_ImGML_String()); return 1.0; }

    // = true
    IMGML IOGetOptCursorBlink() { CheckImGML_Out(); Out_ImGML_Bool(io.ConfigInputTextCursorBlink); return 1.0; }
    IMGML IOSetOptCursorBlink() { CheckImGML_In(); io.ConfigInputTextCursorBlink = In_ImGML_Bool(); return 1.0; }
    
    ////
    // Return the output structure (used by imguigml object)
    DLL_API double GetIOOut() {
        CheckImGML_Out();
        CGMLTypedBuffer buffer(out, false);
        imguigml->UpdateOutput(buffer);
        return 1.0;
    }

    ////
    // IMGUI_API void AddInputCharacter(ImWchar c);                        // Add new character into InputCharacters[]
    // IMGUI_API void AddInputCharactersUTF8(const char* utf8_chars);      // Add new characters into InputCharacters[] from an UTF-8 string
    // (used by imguigml object)
    DLL_API double UpdateCharacterInput(const char *_inputBuffer) {
        CheckImGML();
        auto& IO(ImGui::GetIO());
        IO.AddInputCharactersUTF8(_inputBuffer);
        return 1.0;
    }

    ////
    // inline void    ClearInputCharacters() { InputCharacters[0] = 0; }   // Clear the text input buffer manually
    // (used by imguigml object)
    DLL_API double ClearCharacterInput() {
        CheckImGML();
        auto& IO(ImGui::GetIO());
        IO.ClearInputCharacters();
        return 1.0;
    }

}
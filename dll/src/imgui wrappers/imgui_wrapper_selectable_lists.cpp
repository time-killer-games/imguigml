#include "imguigml_wrappers.h"

extern "C" {
    ////
    // Widgets: Selectable / Lists
    ////
    
    ////
    // IMGUI_API bool          Selectable(const char* label, bool selected = false, ImGuiSelectableFlags flags = 0, const ImVec2& size = ImVec2(0, 0));
    // IMGUI_API bool          Selectable(const char* label, bool* p_selected, ImGuiSelectableFlags flags = 0, const ImVec2& size = ImVec2(0, 0));
    //  size.x==0.0: use remaining width, size.x>0.0: specify width. size.y==0.0: use label height, size.y>0.0: specify height
    IMGML ImSelectable() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        bool selected(In_ImGML_BoolOpt(false));
        int32_t flags(In_ImGML_IntOpt(0));
        float x(In_ImGML_FloatOpt(0.0f));
        float y(In_ImGML_FloatOpt(0.0f));
        bool ret(ImGui::Selectable(label.c_str(), &selected, flags, ImVec2(x, y)));
        
        Out_ImGML_Bool(ret);
        Out_ImGML_Bool(selected);
        return 1.0;
    }

    ////
    // IMGUI_API bool          ListBox(const char* label, int* current_item, const char* const* items, int items_count, int height_in_items = -1);
    IMGML ListBox() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        int currentItem(In_ImGML_Int());
        int itemsCount(In_ImGML_Int());
        
        std::vector<std::string> items;
        for (int i(0); i < itemsCount; ++i)
            items.push_back(In_ImGML_String());

        std::vector<const char*> itemNames;
        for (const auto& item : items)
            itemNames.push_back(item.c_str());
        
        int heightInItems(In_ImGML_IntOpt(-1));
        bool ret(ImGui::ListBox(label.c_str(), &currentItem, itemNames.data(), itemsCount, heightInItems));
        
        Out_ImGML_Bool(ret);
        Out_ImGML_Int(currentItem);

        return 1.0;
    }

    ////
    // IMGUI_API bool          ListBoxHeader(const char* label, const ImVec2& size = ImVec2(0, 0));
    // IMGUI_API bool          ListBoxHeader(const char* label, int items_count, int height_in_items = -1);
    //  use if you want to reimplement ListBox() will custom data or interactions. make sure to call ListBoxFooter() afterwards.
    IMGML ListBoxHeader() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        bool ret(false);
        if (in.IsType<float>(1)) {
            float x(In_ImGML_Float());
            float y(In_ImGML_Float());
            ret = ImGui::ListBoxHeader(label.c_str(), ImVec2(x, y));
        } else {
            int itemsCount(In_ImGML_Int());
            int heightInItems(In_ImGML_IntOpt(-1));
            ret = ImGui::ListBoxHeader(label.c_str(), itemsCount, heightInItems);
        }
        Out_ImGML_Bool(ret);
        return 1.0;
    }

    ////
    // IMGUI_API void          ListBoxFooter();
    //  terminate the scrolling region
    IMGML ListBoxFooter() {
        CheckImGML();
        ImGui::ListBoxFooter();
        return 1.0;
    }
}
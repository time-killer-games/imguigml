#pragma once

#include "types.h"

struct ImDrawData;
struct ImGuiContext;
class IGGRenderBuffer;
class CGMLBuffer;
class CGMLTypedBuffer;


class ImGuiGML {
public:
    class Error : public std::runtime_error {
    public:
        Error();
        Error(const std::string& _what) : std::runtime_error(_what.c_str()) { ; }
    };

public:
    ImGuiGML();
    ~ImGuiGML();

    void SetRenderBuffer(char *_cmdBuffer, uint32_t _cmdBufferSize, char *_vertexBuffer, uint32_t _vertexBufferSize);
    void SetWrapperBuffer(char *_wrapperBuffer, uint32_t _wrapperBufferSize);
    CGMLBuffer& WrapperBuffer() const;

    void SetDisplaySize(float _width, float _height) { mDisplayWidth = _width; mDisplayHeight = _height; }
    void GetDisplaySize(float& _width, float& _height) const { _width = mDisplayWidth; _height = mDisplayHeight; }

    void CreateFontTexture(uint32_t _textId);
    void WriteFont(char* _buffer);
    uint32_t GetFontWidth()  const { return static_cast<uint32_t>(mFontWidth);  }
    uint32_t GetFontHeight() const { return static_cast<uint32_t>(mFontHeight); }

    void UpdateOutput(CGMLTypedBuffer& _buffer);
    void UpdateInput(CGMLBuffer& _inputBuffer);
    void BeginStep(double _deltaTime);
    void EndStep();
    void WaitForBuffer();

    static const std::unique_ptr<ImGuiGML>& Instance();
    static void SetDebugBuffer(char *_wrapperBuffer, uint32_t _wrapperBufferSize);
    static void AssertFailed(bool _cond, const char* _msg, const char *_file, int _line);
    static void FlushDebugBuffer();

private:
    
	ImGuiContext* mContext = nullptr;
    unsigned char* mFontBytes = nullptr;
    int32_t mFontWidth  = 0;
    int32_t mFontHeight = 0;

    float mMouseWheelSpeed = 1.0f;
    std::shared_ptr<IGGRenderBuffer> mRenderBuffer;
    std::shared_ptr<CGMLBuffer>      mWrapperBuffer;
    static std::shared_ptr<CGMLBuffer> mDebugBuffer;

    float mDisplayWidth  = 0.0f;
    float mDisplayHeight = 0.0f;

    SpinLock mRenderLock;
    bool mThreadStarted = false;
    std::atomic<bool> mShuttingDown; 
};

#define IMGUIGML_ASSERT(EXPR)  ImGuiGML::AssertFailed((EXPR), #EXPR, __FILE__, __LINE__)
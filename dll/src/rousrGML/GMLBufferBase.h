#pragma once

#include "rousrGML.h"

class CGMLBufferBase {
public:
    CGMLBufferBase(size_t _bufferSize, const char* _bufferPtr);
    virtual ~CGMLBufferBase() { ; }

    const char*              Raw() const { return mBuffer; }
    char*                    Raw() { return mBuffer; }

    void Reset() { mBufferSize = 0; mSeekPos = 0; mBuffer = nullptr; }

    size_t Size() const { return mBufferSize; }
    size_t Tell() const { return mSeekPos; }
    void   Seek(size_t _pos) { mSeekPos = _pos; }
    void   SeekOffset(size_t _offset) { mSeekPos += _offset; }

    int8_t PeekByte() const;
    int8_t PeekByte(uint32_t _seekPos) const;

    int8_t ReadByte();

    void Clear();
    void WriteData(const char *_data, size_t _size);
 
    template <typename T>
    typename std::enable_if<!std::is_same<T, std::string>::value, T>::type Read() {
        if (mSeekPos + sizeof(T) >= mBufferSize)
            return 0;

        T _val(*reinterpret_cast<T*>(&mBuffer[mSeekPos]));
        mSeekPos += sizeof(T);
        return _val;
    }

    template <typename T>
    typename std::enable_if<std::is_same<T, std::string>::value, T>::type Read() {
        char buffer[rousrGML::MaxGMLStringLen];
        char* val = &buffer[0];

        for (size_t& pos(mSeekPos), end(Size()); pos < end && mBuffer[pos] != 0; ++pos) {
            *val = mBuffer[pos];
            val++;
        }
        *val = 0;
        mSeekPos++; // account for terminator
        mSeekPos = std::min<size_t>(mSeekPos, Size() - 1);

        return buffer;
    }

protected:
    char*  mBuffer;
    size_t mSeekPos;
    size_t mBufferSize;
};
#include "GMLTypedBuffer.h"

#include "rousrGML.h"
#include "GMLBuffer.h"

CGMLTypedBuffer::CGMLTypedBuffer(const CGMLBuffer& _other, bool _readValues) 
    : CGMLBufferBase(_other.Size(), _other.Raw()) { 
    
    mSeekPos = 0;
    if (_readValues)
        ReadValues();
}

CGMLBuffer CGMLTypedBuffer::ToBuffer() const {
    return CGMLBuffer(Size(), Raw());
}

std::shared_ptr<CGMLTypedBuffer::IVal> CGMLTypedBuffer::NextVal() {
    if (mNextVal < mValues.size()) {
        auto val(mValues[mNextVal++]);
        return val;
    }

    return nullptr;
}

void CGMLTypedBuffer::ReadValues() {
    Seek(0);
    mNextVal = 0;
    rousrGML::DataType dataType(static_cast<rousrGML::DataType>(Read<int8_t>()));
    
    while (dataType != rousrGML::DataType::Terminate) {
        switch (dataType) {
        case rousrGML::DataType::Byte:   mValues.push_back(readTVal<char>(dataType)); break;
        case rousrGML::DataType::Bool:   mValues.push_back(readTVal<bool>(dataType)); break;

        case rousrGML::DataType::Int8:   mValues.push_back(readTVal<int8_t>(dataType)); break;
        case rousrGML::DataType::Int16:  mValues.push_back(readTVal<int16_t>(dataType)); break;
        case rousrGML::DataType::Int32:  mValues.push_back(readTVal<int32_t>(dataType)); break;
        case rousrGML::DataType::Int64:  mValues.push_back(readTVal<int64_t>(dataType)); break;

        case rousrGML::DataType::Uint8:  mValues.push_back(readTVal<uint8_t>(dataType)); break;
        case rousrGML::DataType::Uint16: mValues.push_back(readTVal<uint16_t>(dataType)); break;
        case rousrGML::DataType::Uint32: mValues.push_back(readTVal<uint32_t>(dataType)); break;
        case rousrGML::DataType::Uint64: mValues.push_back(readTVal<uint64_t>(dataType)); break;

        case rousrGML::DataType::Float:  mValues.push_back(readTVal<float>(dataType)); break;
        case rousrGML::DataType::Double: mValues.push_back(readTVal<double>(dataType)); break;
        case rousrGML::DataType::String: mValues.push_back(readTVal<std::string>(dataType)); break;

        default:break;
        }

        dataType = static_cast<rousrGML::DataType>(Read<int8_t>());
    }
}
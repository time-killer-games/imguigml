#pragma once

#include "types.h"

#include "GMLBuffer.h"
#include "GMLTypedBuffer.h"

#include "TaskRunner.h"

class GMLSharedCallstack {
public:
    class GMLCall {
    public:
        GMLCall() { ; }
        GMLCall(GMLSharedCallstack* _parent, size_t _callId);
        virtual ~GMLCall();
        
        void Clear() { mTask = nullptr; mBuff.Reset(); mGMLActive = false; }// retain parent and id

        size_t CallId() const { return mCallId; }
        void Call(std::function<void(GMLCall&)>&& _callback);

        CGMLBuffer&     Buffer()       { return mBuff; }
        CGMLTypedBuffer TypedBuffer() { return CGMLTypedBuffer(mBuff); }

        virtual void YieldToGML();
        virtual rousrGML::Status Continue(CGMLBuffer&& _buff);
        
    protected:
        virtual void AllocBuffer() { Wait(rousrGML::Status::Init); } // We just have to wait - continue gives us the buffer.
        virtual void Wait(rousrGML::Status _status = rousrGML::Status::Working);
        virtual void Finish();
        
    private:
        
        std::function<void(GMLCall&)> mCallback = nullptr;

        size_t                  mCallId    = size_t_max;
        TaskRunner::Task*       mTask      = nullptr;
        GMLSharedCallstack*     mParent    = nullptr;
        
        rousrGML::Status        mStatus = rousrGML::Status::Done;
        CGMLBuffer              mBuff;
        std::condition_variable mReady;
        std::mutex              mMutex;
        std::atomic<bool>       mGMLActive = { false };
    };

public:
    GMLSharedCallstack() { ; }
    virtual ~GMLSharedCallstack() { ; }
    
    static double Call(std::function<void(GMLCall&)>&& _callback);
    static double WaitingFor(size_t _callId, CGMLBuffer&& _buff);

private:
    double Call_Impl(std::function<void(GMLCall&)>&& _callback);
    GMLCall* FindCall(size_t _callId);

private:
    size_t mNextId = 0;
    std::stack<size_t> mFreeIndices;

    std::vector<std::unique_ptr<GMLCall>> mCalls;
    TaskRunner mTasks;

    friend class GMLClass;
};
#pragma once

#include "GMLBufferBase.h"

class CGMLBuffer;

class CGMLTypedBuffer : public CGMLBufferBase {
public:
private:
    struct IVal {
        IVal(rousrGML::DataType _type = rousrGML::DataType::Terminate) : type(_type) { ; }
        virtual ~IVal() { ; }
        rousrGML::DataType type;
    };

    template <typename T>
    struct TVal : public IVal {
        TVal(rousrGML::DataType _type, T _val) : IVal(_type), val(_val) { ; }
        T val;
    };

public:
    CGMLTypedBuffer(size_t _bufferSize, const char* _bufferPtr) : CGMLBufferBase(_bufferSize, _bufferPtr) { ReadValues(); }
    CGMLTypedBuffer(const CGMLBuffer& _other, bool _readValues = true);
    virtual ~CGMLTypedBuffer() { ; }

    bool IsType(size_t _index, rousrGML::DataType _type) {
        auto val(mValues[_index]);
        return val->type == _type;
    }

    template <typename T>
    bool IsType(size_t _index) {
        auto val(mValues[_index]);
        std::shared_ptr<TVal<T>> tval(std::dynamic_pointer_cast<TVal<T>>(val));
        return (tval != nullptr);
    }

    template <typename T>
    bool NextType() {
        if (mNextVal == mValues.size())
            return false;
        return IsType<T>(mNextVal);
    }

    template <typename T>
    T NextVal(T _default = T()) {
        return Val<T>(mNextVal++, _default);
    }

    template <typename T>
    T Val(size_t _index, T _default = T()) {
        if (_index < mValues.size()) {
            auto val(mValues[_index]);

            std::shared_ptr<TVal<T>> tval(std::dynamic_pointer_cast<TVal<T>>(val));
            if (tval != nullptr)
                return tval->val;
        }
        return _default;
    }

    template <typename T, typename = std::enable_if_t<!std::is_same<T, std::string>::value>>
    void Write(const T& _val) {
        size_t totalSize(mSeekPos + sizeof(T) + 1);
        if (totalSize >= mBufferSize)
            return;

        mBuffer[mSeekPos++] = static_cast<int8_t>(rousrGML::GetDataType<T>());
        *reinterpret_cast<T*>(&mBuffer[mSeekPos]) = _val;
        mSeekPos += sizeof(T);
    }

    template <typename T, typename = std::enable_if_t<std::is_same<T, std::string>::value>>
    void Write(const std::string& _val) {
        if (mSeekPos + _val.length() + 1 + 1 >= mBufferSize)
            return;

        char* _buffer = &mBuffer[mSeekPos];
        _buffer[0] = static_cast<int8_t>(rousrGML::DataType::String);
        mSeekPos++;
        
        _buffer = &mBuffer[mSeekPos];
        size_t length(_val.length());

        std::memcpy(_buffer, _val.c_str(), length);
        mSeekPos += _val.length();
        mBuffer[mSeekPos++] = 0;
    }

    CGMLBuffer ToBuffer() const;
    size_t Count() const { return mValues.size(); }

    std::shared_ptr<IVal> NextVal();

private:
    void ReadValues();

    template <typename T>
    std::shared_ptr<TVal<T>> readTVal(rousrGML::DataType _type) {
        return std::make_shared<TVal<T>>(_type, Read<T>());
    }


private:
    std::vector<std::shared_ptr<IVal>> mValues;
    size_t mNextVal = 0u;
};
#pragma once

#include "./imguigml.h"

#include "rousrGML.h"

#include "GMLDSMap.h"

//////////////
class IGMLVal {//
public:
	virtual void        WriteToMap(int _mapId, char* _key) = 0; 
	virtual bool        Equals(std::shared_ptr<IGMLVal> _other) = 0;
	virtual bool        Set(std::shared_ptr<IGMLVal> _other) = 0;
    virtual std::shared_ptr<IGMLVal> Clone() const = 0;

protected:
	template <typename T>
	static bool InternalEquals(T& _a, std::shared_ptr<IGMLVal> _b) {
		std::shared_ptr<T> other(std::dynamic_pointer_cast<T>(_b));
		if (other == nullptr)
			return false;

		return _a.Val() == other->Val();
	}
};

//////////////
template <typename T>
class TGMLVal : public IGMLVal {
public:
	TGMLVal(const T& _val) : mVal(_val) { ; }
	TGMLVal() : mVal() { ; }

	T& Val()             { return mVal; }
	const T& Val() const { return mVal; }

	void SetVal(const T& _val) { mVal = _val; }

	void WriteToMap(int _mapId, char* _key) override { CGMLDSMap::WriteDouble(_mapId, _key, static_cast<double>(mVal)); }
    bool Equals(std::shared_ptr<IGMLVal> _other) override { return IGMLVal::InternalEquals<TGMLVal<T>>(*this, _other); }
	std::shared_ptr<IGMLVal> Clone() const override       { return std::make_shared<TGMLVal<T>>(mVal);  }
    bool Set(std::shared_ptr<IGMLVal> _other) override    {
		auto other(std::dynamic_pointer_cast<TGMLVal<T>>(_other));
		if (other != nullptr) {
			mVal = other->mVal;
			return true;
		}

		return false;
	}

private:
	T mVal;
};

//////////////
class CGMLValVoid : public IGMLVal {
public:
	CGMLValVoid(CGMLBuffer* _buffer) { ; }
	CGMLValVoid() { ; }

	bool Equals(std::shared_ptr<IGMLVal> _other) override {
		std::shared_ptr<CGMLValVoid> other(std::dynamic_pointer_cast<CGMLValVoid>(_other));
		return other != nullptr;
	}
	void WriteToMap(int, char*) override { ; }
    std::shared_ptr<IGMLVal> Clone() const override { return std::make_shared<CGMLValVoid>(); }
    bool Set(std::shared_ptr<IGMLVal> _other) override { return std::dynamic_pointer_cast<CGMLValVoid>(_other) != nullptr; }
};

//////////////
template <>
class TGMLVal<std::string> : public IGMLVal {
	static const std::string Empty;

public:
	TGMLVal(const std::string& _val = TGMLVal<std::string>::Empty)
		: mData(_val)
	{ }
	
	char *Val()             { return mData.Val(); }
	const char* Val() const { return mData.Val(); }

	void SetVal(const char* _s) { mData = _s; }
	void SetVal(const std::string& _s) { mData = _s; }

	void WriteToMap(int _mapId, char* _key) override { CGMLDSMap::WriteString(_mapId, _key, mData.Val()); }
	bool Equals(std::shared_ptr<IGMLVal> _other) override {	return IGMLVal::InternalEquals<TGMLVal<std::string>>(*this, _other); }
	bool Set(std::shared_ptr<IGMLVal> _other) override {
		auto other(std::dynamic_pointer_cast<TGMLVal<std::string>>(_other));
		if (other != nullptr) {
			mData = other->mData.Val();  // copy the string
			return true;
		}

		return false;
	}

    std::shared_ptr<IGMLVal> Clone() const override {
        return std::make_shared<TGMLVal<std::string>>(mData.Val());
    }

private:
	// Container class to hold a fixed string
	// DSMap uses a char *, so we need to hold the memory to write to the DSMap
	class ManagedString {
	public:
		ManagedString()                        { memset(mData, 0, sizeof(char)); }
		ManagedString(const std::string& _val) { strncpy_s(mData, _val.c_str(), _val.size()); }
		ManagedString(const ManagedString& _r) { strncpy_s(mData, _r.mData, rousrGML::MaxGMLStringLen); }

		ManagedString& operator=(const std::string& _val) { strncpy_s(mData, _val.c_str(), _val.size()); return *this; }
		ManagedString& operator=(const ManagedString& _r) { strncpy_s(mData, _r.mData, rousrGML::MaxGMLStringLen);     return *this; }

		char*       Val()       { return mData; }
		const char* Val() const { return mData; }

	private:
		char mData[rousrGML::MaxGMLStringLen];
	};
	
	ManagedString  mData;
};
